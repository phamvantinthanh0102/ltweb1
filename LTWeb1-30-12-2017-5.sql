-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 30, 2017 at 07:53 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ltweb1`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `f_ID` int(11) NOT NULL AUTO_INCREMENT,
  `f_Username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `f_Password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `f_Name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `f_Email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `f_DOB` date NOT NULL,
  `f_Address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `f_PhoneNumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `f_Permission` int(11) NOT NULL,
  PRIMARY KEY (`f_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`f_ID`, `f_Username`, `f_Password`, `f_Name`, `f_Email`, `f_DOB`, `f_Address`, `f_PhoneNumber`, `f_Permission`) VALUES
(1, 'a', '46315d1d58cae3d8df137cd2ad9c4a70', 'Phạm Văn Tín Thành', 'abc@gmail.com', '1996-02-01', '2/9 đường 1', '01697005723', 1),
(2, 'b', '46315d1d58cae3d8df137cd2ad9c4a70', 'Thành Phạm', 'def@gmail.com', '1996-05-03', '3/5 đường 2', '012345678', 0),
(3, 'c', '46315d1d58cae3d8df137cd2ad9c4a70', 'Tín Thành', 'ghi@gmail.com', '1991-01-01', '4/3 đường 3', '876543210', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
