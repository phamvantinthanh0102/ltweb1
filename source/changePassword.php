<?php

require_once "./lib/db.php";
require_once "./lib/rememberMe.php";

if(!isset($_SESSION["current_user"]))
{
	header("Location: login.php");
}
else
{
	$changeFailure = 0;
	if(isset($_POST["txtNewPassword"]))
	{
		$oldPassword = $_POST["txtOldPassword"];
		$enc_oldPassword = md5($oldPassword);
		if($enc_oldPassword != $_SESSION["current_user"]->f_Password)
		{
			$changeFailure = 2;
		}
		else
		{
			$id = $_SESSION["current_user"]->f_ID;
			$password = $_POST["txtNewPassword"];
			$enc_password = md5($password);
			$sql = "update users set f_Password = '$enc_password' where f_ID = $id";
			$rs = load($sql);
			if($rs)
			{
				$_SESSION["current_user"]->f_Password = $enc_password;
				$changeFailure = -1;
			}
			else
			{
				$changeFailure = 1;
			}
		}
	}

	$page_title = "Thay đổi mật khẩu";

	$base_filename = basename(__FILE__, '.php');
	$page_body_file = "$base_filename/$base_filename.body.tpl";

	include 'views/_layout.php';
}