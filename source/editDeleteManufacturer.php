<?php

require_once "./lib/db.php";
require_once "./lib/rememberMe.php";

if(!isset($_SESSION["current_user"]))
{
	header("Location: login.php");
}
else
{
	if($_SESSION["current_user"]->f_Permission == 1)
	{
		$page_title = "Xoá, sửa nhà sản xuất";

		$base_filename = basename(__FILE__, '.php');
		$page_body_file = "$base_filename/$base_filename.body.tpl";

		include 'views/_layout.php';
	}
	else
	{
		header("Location: index.php");
	}
}