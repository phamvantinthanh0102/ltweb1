<?php
	require_once "../lib/db.php";
	require_once "../lib/rememberMe.php";

	if(!isset($_SESSION["current_user"]))
	{
		header("Location: ../login.php");
	}
	else
	{
		if(count($_SESSION["cart"]) > 0)
		{
			$userId = $_SESSION["current_user"]->f_ID;
			$sql = "insert into orders(OrderDate, UserID, Total) values(NOW(), '$userId', 0)";
			$orderId = loadWithInsertId($sql);
			$total = 0;
			foreach ($_SESSION["cart"] as $proId => $q) :
				$sql = "select * from products where ProID = $proId";
				$rs = load($sql);
				$row = $rs->fetch_assoc();
				$price = $row["Price"];
				$amount = $q * $price;
				$total += $amount;
				$sql = "update products set Quantity = Quantity - $q, SellCount = SellCount + $q where ProID = $proId";
				load($sql);
				$sql = "insert into orderdetails(OrderID, ProID, Quantity, Price, Amount) values($orderId, $proId, $q, $price, $amount)";
				load($sql);
			endforeach;
			$sql = "update orders set Total = '$total' where OrderID = '$orderId'";
			load($sql);
			$_SESSION["cart"] = array();
		}
		header("Location: ../viewOrders.php?checkOut=success");
	}
?>