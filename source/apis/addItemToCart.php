<?php
	require_once "../lib/db.php";
	require_once "../lib/rememberMe.php";
	require_once "../lib/cart.php";

	if(!isset($_SESSION["current_user"]))
	{
		header("Location: ../login.php");
	}
	else
	{
		if (isset($_POST["txtProID"])) {
			$proId = $_POST["txtProID"];
			$q = $_POST["txtQuantity"];
			
			$sql = "select * from products where ProID = $proId";
			$rs = load($sql);
			$row = $rs->fetch_assoc();
			$quantityMax = $row["Quantity"];
			if(array_key_exists($proId, $_SESSION["cart"]))
			{
				$quantityMax -= $_SESSION["cart"][$proId];
			}
			if($quantityMax > 0)
			{
				add_item($proId, $q);

				if (isset($_SERVER['HTTP_REFERER'])) {
				    $url = $_SERVER['HTTP_REFERER'];
				    header("Location: $url");
				}
			}
			else
			{
				$url = $_SERVER['HTTP_REFERER'];
				header("Location: ../quantityZero.php?url=$url");
			}
			var_dump($row["Quantity"]);
			var_dump($_POST["txtQuantity"]);
			var_dump($_POST["txtProID"]);
		}
	}
?>