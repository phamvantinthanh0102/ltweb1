<?php

require_once "../lib/rememberMe.php";
require_once "../lib/db.php";
require_once "../lib/cart.php";

if (isset($_POST["txtDeleteProID"])) {
	$id = $_POST["txtDeleteProID"];

	$sql = "delete from products where ProID = $id";

	load($sql);

	unlink("../imgs/sp/$id/main.jpg");

	rmdir("../imgs/sp/$id");
	
	delete_item($id);

	header("Location: ../viewProducts.php");
}