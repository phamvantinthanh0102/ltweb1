<?php

require_once "./lib/db.php";
require_once "./lib/rememberMe.php";
require_once "./lib/cart.php";

$page_title = "Giỏ hàng của bạn";

$base_filename = basename(__FILE__, '.php');
$page_body_file = "$base_filename/$base_filename.body.tpl";

include 'views/_layout.php';