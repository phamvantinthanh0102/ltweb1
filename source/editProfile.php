<?php

require_once "./lib/db.php";
require_once "./lib/rememberMe.php";

if(!isset($_SESSION["current_user"]))
{
	header("Location: login.php");
}
else
{
	$editFailure = false;
	if(isset($_POST["txtName"]))
	{
		$id = $_SESSION["current_user"]->f_ID;
		$name = $_POST["txtName"];
		$email = $_POST["txtEmail"];
		$dob = $_POST["txtDOB"];
		$dobFormatted = $_POST["txtDOBFormatted"];
		$address = $_POST["txtAddress"];
		$phoneNumber = $_POST["txtPhoneNumber"];
		$sql = "update users set f_Name = '$name', f_Email = '$email', f_DOB = '$dobFormatted', f_Address = '$address', f_PhoneNumber = '$phoneNumber' where f_ID = $id";
		$rs = load($sql);
		if($rs)
		{			
			$_SESSION["current_user"]->f_Name = $name;
			$_SESSION["current_user"]->f_Email = $email;
			$_SESSION["current_user"]->f_DOB = $dob;
			$_SESSION["current_user"]->f_Address = $address;
			$_SESSION["current_user"]->f_PhoneNumber = $phoneNumber;
			header("Location: profile.php");
		}
		else
		{
			$editFailure = true;
		}
	}

	$page_title = "Chỉnh sửa thông tin cá nhân";

	$base_filename = basename(__FILE__, '.php');
	$page_body_file = "$base_filename/$base_filename.body.tpl";

	include 'views/_layout.php';
}