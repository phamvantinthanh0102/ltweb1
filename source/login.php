<?php
	require_once "./lib/db.php";
	require_once "./lib/rememberMe.php";
	
	$loginFailed = 0;
	if (isset($_POST["txtUserName"])) {
		$username = $_POST["txtUserName"];
		$password = $_POST["txtPassword"];
		$enc_password = md5($password);
		$sql = "select * from users where f_Username = '$username' and f_Password = '$enc_password'";
		$rs = load($sql);
		if ($rs->num_rows > 0) {
			$_SESSION["current_user"] = $rs->fetch_object();

			$oldDOB = strtotime($_SESSION["current_user"]->f_DOB);

			$_SESSION["current_user"]->f_DOB = date("m/d/Y", $oldDOB);

			if(isset($_POST["cbRemember"])) {
				$user_id = $_SESSION["current_user"]->f_ID;
				setcookie("current_user", $user_id, time() + 86400);
			} 
		} else {
			$loginFailed = 1;
		}
	}

$page_title = "Đăng nhập";

$base_filename = basename(__FILE__, '.php');
$page_body_file = "$base_filename/$base_filename.body.tpl";

include 'views/_layout.php';

?>