<?php

require_once "./lib/db.php";
require_once "./lib/rememberMe.php";
require_once './vendor/autoload.php';

if(isset($_SESSION["current_user"]))
{
	header("Location: index.php");
}
else
{
	$signupFailure = false;
	if(isset($_POST["txtCaptcha"]))
	{
		$username = $_POST["txtUsername"];
		$password = $_POST["txtPassword"];
		$enc_password = md5($password);
		$name = $_POST["txtName"];
		$email = $_POST["txtEmail"];
		$dob = $_POST["txtDOBFormatted"];
		$address = $_POST["txtAddress"];
		$phoneNumber = $_POST["txtPhoneNumber"];
		$sql = "insert into users(f_Username, f_Password, f_Name, f_Email, f_DOB, f_Address, f_PhoneNumber, f_Permission) values('$username', '$enc_password', '$name', '$email', '$dob', '$address', '$phoneNumber', 0)";
		$rs = load($sql);
		if($rs)
		{
			header("Location: signupSuccess.php");
		}
		else
		{
			$signupFailure = true;
		}
	}

	$page_title = "Đăng ký tài khoản";

	$base_filename = basename(__FILE__, '.php');
	$page_body_file = "$base_filename/$base_filename.body.tpl";

	include 'views/_layout.php';
}