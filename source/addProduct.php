<?php

require_once "./lib/db.php";
require_once "./lib/rememberMe.php";

if(!isset($_SESSION["current_user"]))
{
	header("Location: login.php");
}
else
{
	if($_SESSION["current_user"]->f_Permission == 1)
	{
		$addFailure = false;

		if(isset($_POST["txtName"]))
		{
			$name = $_POST["txtName"];
			$price = $_POST["txtPrice"];
			$quantity = $_POST["txtQuantity"];
			$source = $_POST["txtSource"];
			$des = $_POST["txtDes"];
			$catId = $_POST["selCategory"];
			$manId = $_POST["selManufacturer"];
			$sql = "insert into products(ProName, Price, Quantity, ProSource, Des, CatID, ManID, StartDate, SellCount, ViewCount) values('$name', $price, $quantity, '$source', '$des', $catId, $manId, NOW(), 0, 0)";
			$rs = loadWithInsertId($sql);
			if($rs > 0)
			{
				$f = $_FILES["fuImage"];
				mkdir("imgs/sp/$rs");
				$tmp_name = $f["tmp_name"];
				$destination = "imgs/sp/$rs/main.jpg";
				move_uploaded_file($tmp_name, $destination);

				header("Location: viewProducts.php");
			}
			else
			{
				$addFailure = true;
			}
		}

		$page_title = "Thêm sản phẩm";

		$base_filename = basename(__FILE__, '.php');
		$page_body_file = "$base_filename/$base_filename.body.tpl";

		include 'views/_layout.php';
	}
	else
	{
		header("Location: index.php");
	}
}