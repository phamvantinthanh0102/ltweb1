<?php

require_once "./lib/db.php";
require_once "./lib/rememberMe.php";

if(!isset($_SESSION["current_user"]))
{
	header("Location: login.php");
}
else
{
	if($_SESSION["current_user"]->f_Permission == 1 && isset($_GET["id"]))
	{
		$editFailure = false;

		if(isset($_POST["txtName"]))
		{
			$id = $_POST["txtID"];
			$name = $_POST["txtName"];
			$price = $_POST["txtPrice"];
			$quantity = $_POST["txtQuantity"];
			$source = $_POST["txtSource"];
			$des = $_POST["txtDes"];
			$catId = $_POST["selCategory"];
			$manId = $_POST["selManufacturer"];
			$sql = "update products set ProName = '$name', Price = $price, Quantity = $quantity, ProSource = '$source', Des = '$des', CatID = $catId, ManID = $manId where ProID = $id";
			$rs = load($sql);

			$f = $_FILES["fuImage"];
			if($f["error"] == 0)
			{
				$tmp_name = $f["tmp_name"];
				$destination = "imgs/sp/$id/main.jpg";
				move_uploaded_file($tmp_name, $destination);				
			}

			if($rs == true)
			{
				header("Location: viewProductDetails.php?id=$id");
			}
			else
			{
				$editFailure = true;
			}
		}

		$page_title = "Chỉnh sửa sản phẩm";

		$base_filename = basename(__FILE__, '.php');
		$page_body_file = "$base_filename/$base_filename.body.tpl";

		include 'views/_layout.php';
	}
	else
	{
		header("Location: index.php");
	}
}