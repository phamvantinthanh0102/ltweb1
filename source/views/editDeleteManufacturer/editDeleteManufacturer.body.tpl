<?php
	$updateSuccess = 0;
	$deleteSuccess = 0;
	$manId = "";
	$manName = "";
	if (isset($_POST["txtCmd"])) {
		$cmd = $_POST["txtCmd"];
		$manId = $_POST["txtManId"];
		$manName = $_POST["txtManName"];

		if ($cmd == "D") {
			$sql = "delete from manufacturers where ManID = $manId";
			$rs = load($sql);
			if($rs == true)
			{
				$deleteSuccess = 1;
			}
		} else { // $cmd == "U"
			$sql = "update manufacturers set ManName = '$manName' where ManID = $manId";
			$rs = load($sql);
			if($rs == true)	
			{
				$updateSuccess = 1;
			}
		}
	}
	if($deleteSuccess == 1) :
?>
<div class="alert alert-success" role="alert">Bạn đã xóa thành công nhà sản xuất <b><?= $manName ?></b>!</div>
<?php
	endif;
	if($updateSuccess == 1) :
?>
<div class="alert alert-success" role="alert">Bạn đã cập nhật thành công nhà sản xuất <b><?= $manName ?></b>!</div>
<?php
	endif;
?>
<form id="f" method="post" action="">
	<input type="hidden" id="txtCmd" name="txtCmd">
	<input type="hidden" id="txtManId" name="txtManId">
	<input type="hidden" id="txtManName" name="txtManName">
</form>
<table class="table table-hover">
	<thead>
		<tr>
			<th>Mã nhà sản xuất</th>
			<th>Tên nhà sản xuất</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	<?php
		$sql = "select * from manufacturers";
		$rs = load($sql);
		if ($rs->num_rows > 0) :
			while ($row = $rs->fetch_assoc()) :
	?>
		<tr>
			<td><?= $row["ManID"] ?></td>
			<td><input type="text" class="form-control editManName" value="<?= $row["ManName"] ?>"></td>
			<td class="text-right">
				<a class="btn btn-xs btn-danger manufacturer-remove" data-id="<?= $row["ManID"] ?>" href="javascript:;" role="button">
					<span class="glyphicon glyphicon-remove"></span>
				</a>
				<a class="btn btn-xs btn-primary manufacturer-update" data-id="<?= $row["ManID"] ?>" href="javascript:;" role="button">
					<span class="glyphicon glyphicon-ok"></span>
				</a>
			</td>
		</tr>
	<?php
			endwhile;
		endif;
	?>
	</tbody>
</table>

<a href="adminDashboard.php" class="btn btn-default">
	<span class="glyphicon glyphicon-chevron-left"></span>
	Quay lại
</a>

<script type="text/javascript">
	$('.manufacturer-remove').on('click', function() {
		
		var s = $(this).closest('tr').find('.editManName').val();
		$('#txtManName').val(s);
		$('#modal-man-name').html(s);

		var id = $(this).data('id');
		$('#txtManId').val(id);
	    $('#txtCmd').val('D');

	    $('#delete-modal').modal("show");
	});

	$('.manufacturer-update').on('click', function() {

		var s = $(this).closest('tr').find('.editManName').val();
		$('#txtManName').val(s);

		var id = $(this).data('id');
		$('#txtManId').val(id);
	    $('#txtCmd').val('U');

	    $('#f').submit();
	});
</script>

<!-- Modal -->
<div class="modal fade" id="delete-modal" role="dialog">
    <div class="modal-dialog">
    
    	<!-- Modal content-->
    	<div class="modal-content">
        	<div class="modal-header">
          		<button type="button" class="close" data-dismiss="modal">&times;</button>
          		<h4 class="modal-title">Xóa nhà sản xuất</h4>
        	</div>
        <div class="modal-body">
          	<p>Bạn có chắc muốn xóa nhà sản xuất <b id="modal-man-name"></b>?</p>
        </div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-default" id="modal-yes-button">Có</button>
          	<button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
        </div>
      </div>
      
    </div>
</div>

<script type="text/javascript">
	$("#modal-yes-button").on('click', function() {
		$('#f').submit();
	});
</script>