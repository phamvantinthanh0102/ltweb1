﻿<?php
	require_once "./lib/rememberMe.php";
	require_once "./lib/db.php";
	require_once "./lib/cart.php";
?>

<!DOCTYPE html>
<html>
<head>
	<title>Siêu thị máy ảnh số</title>
	<link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.7-dist/css/bootstrap.min.css">
	<meta charset="utf-8">
</head>
<body>
	<script src="assets/jquery-3.1.1.min.js"></script>
	<script src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.php">
					Trang chủ
					<span class="glyphicon glyphicon-home"></span>
				</a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="search.php">Tìm kiếm<span style="margin-left:10px;" class="glyphicon glyphicon-search"></span></a></li>
				<?php
					if(!isset($_SESSION["current_user"])) :
				?>
					<li><a href="signup.php">Đăng ký tài khoản<span style="margin-left:10px;" class="glyphicon glyphicon-pencil"></span></a></li>
				<?php
					else :
						if($_SESSION["current_user"]->f_Permission == 0) :
				?>
					<li><a href="viewOrders.php">Lịch sử mua hàng<span style="margin-left:10px;" class="glyphicon glyphicon-tag"></span></a></li>
				<?php
						else :
				?>
					<li><a href="viewOrders.php">Danh sách đơn hàng<span style="margin-left:10px;" class="glyphicon glyphicon-tag"></span></a></li>
					<li><a href="adminDashboard.php">Dashboard<span style="margin-left:10px;" class="glyphicon glyphicon-list-alt"></span></a></li>
				<?php
						endif;
					endif;
				?>
				</ul>
				<ul class="nav navbar-nav navbar-right">
				<?php
					if(isset($_SESSION["current_user"])) :
				?>
					<li>
						<a href="viewCart.php">
							<span class="glyphicon glyphicon-shopping-cart"></span>
							Giỏ hàng (<?= get_total_items() ?>)
						</a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><b><?= $_SESSION["current_user"]->f_Name ?></b><span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="profile.php">Thông tin cá nhân</a></li>
							<li><a href="changePassword.php">Đổi mật khẩu</a></li>
							<!--<li><a href="#">Đổi mật khẩu</a></li>-->
							<li role="separator" class="divider"></li>
							<li><a href="logout.php">Đăng xuất</a></li>
						</ul>
					</li>
					<?php
						else :
					?>
					<li><a href="login.php"><b>Đăng nhập</b></a></li>
					<?php
						endif;
					?>

				</ul>
			</div>
		</div>
	</nav>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Danh mục</h3>
					</div>
					<div class="list-group">
					<?php
						$sql = "select * from categories";
						$rs = load($sql);
						while ($row = $rs->fetch_assoc()) :
					?>
							<a href="productsByCat.php?id=<?= $row["CatID"] ?>" class="list-group-item"><?= $row["CatName"] ?></a>
					<?php
						endwhile;
					?>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Nhà sản xuất</h3>
					</div>
					<div class="list-group">
					<?php
						$sql = "select * from manufacturers";
						$rs = load($sql);
						while ($row = $rs->fetch_assoc()) :
					?>
							<a href="productsByManufacturer.php?id=<?= $row["ManID"] ?>" class="list-group-item"><?= $row["ManName"] ?></a>
					<?php
						endwhile;
					?>
					</div>
				</div>
			</div>
			<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title"><?= $page_title ?></h3>
					</div>
					<div class="panel-body">
						<?php include_once $page_body_file; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>