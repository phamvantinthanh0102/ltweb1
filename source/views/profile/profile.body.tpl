<div class="col-md-12">
	<b>Tên tài khoản: </b><?= $_SESSION["current_user"]->f_Username ?>
</div>
<div class="col-md-12">
	<b>Họ tên: </b><?= $_SESSION["current_user"]->f_Name ?>
</div>
<div class="col-md-12">
	<b>Email: </b><?= $_SESSION["current_user"]->f_Email ?>
</div>
<div class="col-md-12">
	<b>Ngày sinh: </b><?= $_SESSION["current_user"]->f_DOB ?>
</div>
<div class="col-md-12">
	<b>Địa chỉ: </b><?= $_SESSION["current_user"]->f_Address ?>
</div>
<div class="col-md-12">
	<b>Số điện thoại: </b><?= $_SESSION["current_user"]->f_PhoneNumber ?>
</div>
<div class="col-md-2">
	<br>
	<a href="editProfile.php" class="btn btn-success btn-block">
		Chỉnh sửa
		<span class="glyphicon glyphicon-pencil"></span>
	</a>
</div>