
<div class="row">
<?php
	if(isset($_POST["btnAddCategory"])) :
		$categoryName = $_POST["txtCategory"];
		$sql = "insert into categories(CatName) values('$categoryName')";
		$rs = load($sql);
		if($rs == true) :
?>
	<div class="col-md-12">
		<div class="alert alert-success" role="alert"><b>Chúc mừng!</b> Bạn đã thêm thành công loại sản phẩm <b><?= $categoryName ?></b>!</div>
	</div>
<?php
		else :
?>
	<div class="col-md-12">
		<div class="alert alert-danger" role="alert"><b>Lỗi!</b> Quá trình thêm loại sản phẩm <b><?= $categoryName ?></b> đã thất bại!</div>
	</div>
<?php
		endif;
	endif;
?>
	<div class="col-md-6 col-md-offset-3">
		<form method="post" action="">
			<div class="form-group">
				<label for="txtCategory">Tên loại sản phẩm</label>
				<input type="text" class="form-control" id="txtCategory" name="txtCategory">
			</div>
			<button type="submit" class="btn btn-success btn-block" name="btnAddCategory">
				<span class="glyphicon glyphicon-ok"></span>
				Thêm loại sản phẩm
			</button>
			<a href="adminDashboard.php" class="btn btn-default btn-block">
				<span class="glyphicon glyphicon-chevron-left"></span>
				Quay lại
			</a>
		</form>
	</div>
</div>