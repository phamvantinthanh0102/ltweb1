<div class="col-md-12">
	<table class="table table-hover">
		<thead>
			<tr>
				<th class="col-md-1">Mã sản phẩm</th>
				<th class="col-md-4">Tên sản phẩm</th>
				<th class="col-md-1">Ngày tạo</th>
				<th class="col-md-2">Giá</th>
				<th class="col-md-1">Số lượng</th>
				<th class="col-md-2">&nbsp;</th>
			</tr>
		</thead>
		<tbody>
		<?php
		$sql = "select * from products";
		$rs = load($sql);
			while ($row = $rs->fetch_assoc()) :
				$productDate = strtotime($row["StartDate"]);
		?>
			<tr>
				<td><?= $row["ProID"] ?></td>
				<td><?= $row["ProName"] ?></td>
				<td><?= date("j/n/Y", $productDate) ?></td>
				<td><?= number_format($row["Price"]) ?></td>
				<td><?= $row["Quantity"] ?></td>
				<td class="text-right">
					<a href="viewProductDetails.php?id=<?= $row["ProID"] ?>" class="btn btn-xs btn-info" role="button">
						<span class="glyphicon glyphicon-th"></span>
						xem, xóa, sửa
					</a>
				</td>
			</tr>
		<?php
			endwhile;
		?>
		</tbody>
	</table>
</div>