<?php
	if($addFailure) :
?>
<div id="add-failure" class="alert alert-danger" role="alert">Đã xảy ra lỗi khi thêm sản phẩm.</div>
<?php
	endif;
?>

<div id="invalid-name" style="display:none;" class="alert alert-danger" role="alert">Tên sản phẩm trống.</div>
<div id="invalid-source" style="display:none;" class="alert alert-danger" role="alert">Xuất xứ trống.</div>
<div id="invalid-des" style="display:none;" class="alert alert-danger" role="alert">Mô tả trống.</div>
<div id="invalid-image" style="display:none;" class="alert alert-danger" role="alert">Ảnh sản phẩm chưa được chọn.</div>
<div id="product-existed" style="display:none;" class="alert alert-danger" role="alert">Sản phẩm cùng tên đã tồn tại.</div>

<form id="f" class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
	<div class="form-group">
		<label for="txtName" class="col-sm-2 control-label">Tên sản phẩm</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" id="txtName" name="txtName">
		</div>
	</div>
	<div class="form-group">
		<label for="txtPrice" class="col-sm-2 control-label">Giá</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" id="txtPrice" name="txtPrice" value="50000">
		</div>
	</div>
	<div class="form-group">
		<label for="txtQuantity" class="col-sm-2 control-label">Số lượng</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" id="txtQuantity" name="txtQuantity" value="1">
		</div>
	</div>
	<div class="form-group">
		<label for="txtSource" class="col-sm-2 control-label">Xuất xứ</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" id="txtSource" name="txtSource">
		</div>
	</div>
	<div class="form-group">
		<label for="txtDes" class="col-sm-2 control-label">Mô tả</label>
		<div class="col-sm-8">
			<textarea rows="6" id="txtDes" name="txtDes" class="form-control"></textarea>
		</div>
	</div>
	<div class="form-group">
		<label for="selCategory" class="col-sm-2 control-label">Loại sản phẩm</label>
		<div class="col-sm-8">
			<select name="selCategory" class="form-control">
			<?php
				$sql = "select * from categories";
				$rs = load($sql);
				while ($row = $rs->fetch_assoc()) :
			?>
				<option value="<?= $row["CatID"] ?>"><?= $row["CatName"] ?></option>
			<?php
				endwhile;
			?>		
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="selManufacturer" class="col-sm-2 control-label">Nhà sản xuất</label>
		<div class="col-sm-8">
			<select name="selManufacturer" class="form-control">
			<?php
				$sql = "select * from manufacturers";
				$rs = load($sql);
				while ($row = $rs->fetch_assoc()) :
			?>
				<option value="<?= $row["ManID"] ?>"><?= $row["ManName"] ?></option>
			<?php
				endwhile;
			?>		
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="fuImage" class="col-sm-2 control-label">Ảnh sản phẩm</label>
		<div class="col-sm-8">
			<input type="file" class="form-control" id="fuImage" name="fuImage">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button name="btnAdd" type="submit" class="btn btn-success">
				<span class="glyphicon glyphicon-ok"></span>
				Thêm sản phẩm
			</button>
		</div>
	</div>
</form>
<script src="assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	tinymce.init({
	    selector: '#txtDes',
	    menubar: false,
	    toolbar1: "styleselect | bold italic | link image | alignleft aligncenter alignright | bullist numlist | fontselect | fontsizeselect | forecolor backcolor",
	    // toolbar2: "",
	    // plugins: 'link image textcolor',
	    //height: 300,
	    // encoding: "xml",
	});
</script>
<script src="assets/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
<script type="text/javascript">
	$(function () {
		$('#txtPrice').TouchSpin({
			min: 50000,
			max: 100000000,
			step: 50000
			// decimals: 0,
			// boostat: 5,
			// maxboostedstep: 10,
			// postfix: '%'
		});
		$('#txtQuantity').TouchSpin({
			min: 1,
			max: 100
			// step: 1,
			// decimals: 0,
			// boostat: 5,
			// maxboostedstep: 10,
			// postfix: '%'
		});
	});
</script>
<script type="text/javascript">
	$('#f').on('submit', function (e) {
		e.preventDefault();

		var invalid = false;
		var form = this;

		var name = $('#txtName').val();
		if (name.length == 0)
		{
			$("#invalid-name").show();
			invalid = true;
		}
		else
		{
			$("#invalid-name").hide();	
		}
		
		var source = $("#txtSource").val();
		var sourcePattern = /^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]+$/;
		if (source.length == 0 || !sourcePattern.test(source)) {
			$("#invalid-source").show();
			invalid = true;
		}
		else
		{
			$("#invalid-source").hide();	
		}

		var des = tinymce.activeEditor.getBody();
		if (des.length == 0) {
			$("#invalid-des").show();
			invalid = true;
		}
		else
		{
			$("#invalid-des").hide();	
		}


		if($('#fuImage').get(0).files.length == 0) 
		{
			$("#invalid-image").show();
			invalid = true;
		}
		else
		{
			$("#invalid-image").hide();
		}

		var url = 'apis/checkProduct.php?product=' + name;
		$.getJSON(url, function (data) {
			if (data == 1) {				
				$('#product-existed').show();
			} else {
				$('#product-existed').hide();
				if(invalid == false)
				{
					form.submit();
				}
			}
		});
	});
</script>