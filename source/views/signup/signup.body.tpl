
<?php
	if($signupFailure) :
?>
<div id="signup-failure" class="alert alert-danger" role="alert">Đã xảy ra lỗi khi đăng ký tài khoản.</div>
<?php
	endif;
?>

<?php
	use Gregwar\Captcha\CaptchaBuilder;
?>

<div id="invalid-username" style="display:none;" class="alert alert-danger" role="alert">Tên đăng nhập chỉ được chứa kí tự chữ và số và phải bắt đầu bằng ký tự chữ.</div>
<div id="invalid-password" style="display:none;" class="alert alert-danger" role="alert">Mật khẩu phải chứa ít nhất một ký tự thường, một ký tự hoa, và số.</div>
<div id="invalid-confirm-password" style="display:none;" class="alert alert-danger" role="alert">Mật khẩu xác nhận không khớp.</div>
<div id="invalid-name" style="display:none;" class="alert alert-danger" role="alert">Họ tên không hợp lệ.</div>
<div id="invalid-email" style="display:none;" class="alert alert-danger" role="alert">Email không hợp lệ.</div>
<div id="invalid-dob" style="display:none;" class="alert alert-danger" role="alert">Ngày sinh không hợp lệ.</div>
<div id="invalid-address" style="display:none;" class="alert alert-danger" role="alert">Địa chỉ trống.</div>
<div id="invalid-phone-number" style="display:none;" class="alert alert-danger" role="alert">Số điện thoại không hợp lệ.</div>
<div id="invalid-captcha" style="display:none;" class="alert alert-danger" role="alert">Captcha không đúng.</div>
<div id="user-existed" style="display:none;" class="alert alert-danger" role="alert">Tên tài khoản đã được sử dụng.</div>

<form id="f" class="form-horizontal" method="POST" action="">
	<div class="form-group">
		<label for="txtUsername" class="col-sm-2 control-label">Tên đăng nhập</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" id="txtUsername" name="txtUsername">
		</div>
	</div>
	<div class="form-group">
		<label for="txtPassword" class="col-sm-2 control-label">Mật khẩu</label>
		<div class="col-sm-8">
			<input type="password" class="form-control" id="txtPassword" name="txtPassword">
		</div>
	</div>
	<div class="form-group">
		<label for="txtConfirmPassword" class="col-sm-2 control-label">Nhập lại mật khẩu</label>
		<div class="col-sm-8">
			<input type="password" class="form-control" id="txtConfirmPassword" name="txtConfirmPassword">
		</div>
	</div>
	<div class="form-group">
		<label for="txtName" class="col-sm-2 control-label">Họ tên</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" id="txtName" name="txtName">
		</div>
	</div>
	<div class="form-group">
		<label for="txtEmail" class="col-sm-2 control-label">Email</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" id="txtEmail" name="txtEmail">
		</div>
	</div>
	<div class="form-group">
		<label for="txtDOB" class="col-sm-2 control-label">Ngày sinh</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" id="txtDOB" name="txtDOB">
		</div>
	</div>
	<input type="hidden" class="form-control" id="txtDOBFormatted" name="txtDOBFormatted">
	<div class="form-group">
		<label for="txtAddress" class="col-sm-2 control-label">Địa chỉ</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" id="txtAddress" name="txtAddress">
		</div>
	</div>
	<div class="form-group">
		<label for="txtPhoneNumber" class="col-sm-2 control-label">Số điện thoại</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" id="txtPhoneNumber" name="txtPhoneNumber">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<?php
				$builder = new CaptchaBuilder;
				$builder->build();
				$_SESSION["captcha"] = $builder->getPhrase();
			?>
			<img src="<?= $builder->inline() ?>" alt="captcha" />
		</div>
	</div>
	<div class="form-group">
		<label for="txtCaptcha" class="col-sm-2 control-label">Nhập capcha</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" id="txtCaptcha" name="txtCaptcha">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button name="btnSignup" type="submit" class="btn btn-success">
				<span class="glyphicon glyphicon-ok"></span>
				Đăng ký
			</button>
		</div>
	</div>
</form>
<script src="lib/MySQLDate.js"></script>
<script type="text/javascript">
	$('#f').on('submit', function (e) {
		$('#signup-failure').hide();
		e.preventDefault();

		var invalid = false;
		var form = this;

		var username = $('#txtUsername').val();
		var usernamePattern = /^[a-zA-Z][a-zA-Z0-9]*$/;
		if (username.length == 0 || usernamePattern.test(username) == false) {
			$("#invalid-username").show();
			invalid = true;
		}
		else
		{
			$("#invalid-username").hide();	
		}

		var passwordPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{0,}$/;
		var password = $('#txtPassword').val();
		if (password.length == 0 || passwordPattern.test(password) == false) {
			$("#invalid-password").show();
			invalid = true;
		}
		else
		{
			$("#invalid-password").hide();	
		}

		var confirmPassword = $('#txtConfirmPassword').val();
		if (confirmPassword != password) {
			$("#invalid-confirm-password").show();
			invalid = true;
		}
		else
		{
			$("#invalid-confirm-password").hide();	
		}

		var namePattern = /^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]+$/;
		var name = $('#txtName').val();
		if (name.length == 0 || namePattern.test(name) == false) {
			$("#invalid-name").show();
			invalid = true;
		}
		else
		{
			$("#invalid-name").hide();	
		}

		var emailPattern = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)/;
		var email = $('#txtEmail').val();
		if (email.length == 0 || emailPattern.test(email) == false) {
			$("#invalid-email").show();
			invalid = true;
		}		
		else
		{
			$("#invalid-email").hide();	
		}

		var dob = $('#txtDOB').val();
		var checkDate = isNaN(Date.parse(dob));
		if(checkDate == true)
		{
			$("#invalid-dob").show();
			invalid = true;
		}
		else
		{
			var dobFormatted = new Date(dob);
			dobFormatted = toMySQLDateFormat(dobFormatted);
			$("#txtDOBFormatted").val(dobFormatted);
			$("#invalid-dob").hide();	
		}

		var address = $('#txtAddress').val();
		if(address.length == 0)
		{
			$('#invalid-address').show();
			invalid = true;
		}
		else
		{
			$('#invalid-address').hide();
		}

		var phoneNumber = $('#txtPhoneNumber').val();
		var phoneNumberPattern = /^[0-9]*$/;
		if (phoneNumber.length == 0 || phoneNumberPattern.test(phoneNumber) == false) {
			$("#invalid-phone-number").show();
			invalid = true;
		}
		else
		{
			$("#invalid-phone-number").hide();	
		}

		var captcha = $('#txtCaptcha').val();
		var url = 'apis/checkCaptcha.php?captcha=' + captcha;
		$.getJSON(url, function (data) {
			if (data == 1) {
				$('#invalid-captcha').hide();
			} else {
				$('#invalid-captcha').show();
				invalid = true;
			}
		});

		var url2 = 'apis/checkUser.php?user=' + username;
		$.getJSON(url2, function (data) {
			if (data == 1) {				
				$('#user-existed').show();
			} else {
				$('#user-existed').hide();
				if(invalid == false)
				{
					form.submit();
				}
			}
		});
	});
</script>