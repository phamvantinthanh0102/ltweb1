<?php
	if(count($_SESSION["cart"]) > 0) :
?>
	<form id="f" method="post" action="apis/updateCart.php">
		<input type="hidden" id="txtCmd" name="txtCmd">
		<input type="hidden" id="txtDProId" name="txtDProId">
		<input type="hidden" id="txtUQ" name="txtUQ">
	</form>
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Sản phẩm</th>
				<th>Giá</th>
				<th class="col-md-2">Số lượng</th>
				<th>Thành tiền</th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
		<?php
			$total = 0;
			foreach ($_SESSION["cart"] as $proId => $q) :
				$sql = "select * from products where ProID = $proId";
				$rs = load($sql);
				$row = $rs->fetch_assoc();
				$amount = $q * $row["Price"];
				$total += $amount;
		?>
			<tr>
				<td><?= $row["ProName"] ?></td>
				<td><?= number_format($row["Price"]) ?></td>
				<!-- <td><?= $q ?></td> -->
				<td>
					<input class="quantity-textfield" type="text" name="" id="" value="<?= $q ?>">
				</td>
				<td><?= number_format($amount) ?></td>
				<td class="text-right">
					<a class="btn btn-xs btn-danger cart-remove" data-id="<?= $proId ?>" href="javascript:;" role="button">
						<span class="glyphicon glyphicon-remove"></span>
					</a>
					<a class="btn btn-xs btn-primary cart-update" data-id="<?= $proId ?>" href="javascript:;" role="button">
						<span class="glyphicon glyphicon-ok"></span>
					</a>
				</td>
			</tr>
		<?php
			endforeach;
		?>
		</tbody>
		<tfoot>
			<td style="width: 25%;">
			<!--
				<a class="btn btn-success" href="#" role="button">
					<span class="glyphicon glyphicon-backward"></span>
					Tiếp tục mua hàng
				</a>
			-->
			</td>
			<td style="width: 20%;">&nbsp;</td>
			<td style="width: 20%;">&nbsp;</td>
			<td><b><?= number_format($total) ?></b></td>
			<td class="text-right">
				<a class="btn btn-primary" href="apis/checkOut.php" role="button">
					<span class="glyphicon glyphicon-bell"></span>
					Thanh toán
				</a>
			</td>
		</tfoot>
	</table>
	<script src="assets/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
	<script type="text/javascript">
		$('.cart-remove').on('click', function() {
			var id = $(this).data('id');
			$('#txtDProId').val(id);
		    $('#txtCmd').val('D');
		    $('#f').submit();
		});

		$('.cart-update').on('click', function() {

			var q = $(this).closest('tr').find('.quantity-textfield').val();
			$('#txtUQ').val(q);

			var id = $(this).data('id');
			$('#txtDProId').val(id);
		    $('#txtCmd').val('U');

		    $('#f').submit();
		});

		var $listQuantityMax = [
		<?php
			$listProId = array_keys($_SESSION["cart"]);
			for($i = 0; $i < count($listProId); $i++) :
				$proId = $listProId[$i];
				$sql = "select quantity from products where ProID = $proId";
				$rs = load($sql);
				$row = $rs->fetch_assoc();
				echo $row["quantity"];
				if($i < (count($listProId) - 1))
				{
					echo ",";
				}
			endfor;
		?>
		];

		var $listQuantityTextField = $('.quantity-textfield');
		$listQuantityTextField.each(function(index) {
			var $maxValue = $listQuantityMax[index];
			$(this).TouchSpin({
		        min: 1,
		        max: $maxValue
	            // step: 1,
	            // decimals: 0,
	            // boostat: 5,
	            // maxboostedstep: 10,
	            // postfix: '%'
	   		});
		});

	</script>

<?php
	else :
?>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			Không có sản phẩm nào trong giỏ hàng của bạn.
		</div>
	</div>
<?php
	endif;
?>
