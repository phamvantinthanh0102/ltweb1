<?php
	$sql = "select max(price) as HighestPrice from products";
	$rs = load($sql);
	$row = $rs->fetch_assoc();
	$highestPrice = $row["HighestPrice"];
?>

<div class="row">
	
	<div class="col-md-12">

	  	<div class="panel-body">
			<form class="form-horizontal" method="GET" action="">

				<div class="form-group">
					<label for="txtPrice" class="col-sm-2 control-label">Tên sản phẩm:</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="txtName" name="txtName" placeholder="Tên sản phẩm">
					</div>
				</div>

				<div class="form-group">
					<label for="selCategories" class="col-sm-2 control-label">Loại:</label>
					<div class="col-sm-10">
						<select name="selCategories" class="form-control" id="selCategories">
							<option value="0">(trống)</option>
							<?php
								$sql = "select * from categories";
								$rs = load($sql);
								while ($row = $rs->fetch_assoc()) :
							?>
								<option value="<?= $row["CatID"] ?>"><?= $row["CatName"] ?></option>
							<?php
								endwhile;
							?>							  
						</select>
					</div>
				</div>

				<div class="form-group">
					<label for="selManufacturers" class="col-sm-2 control-label">Nhà sản xuất:</label>
					<div class="col-sm-10">
						<select name="selManufacturers" class="form-control" id="selManufacturers">
							<option value="0">(trống)</option>
							<?php
								$sql = "select * from manufacturers";
								$rs = load($sql);
								while ($row = $rs->fetch_assoc()) :
							?>
								<option value="<?= $row["ManID"] ?>"><?= $row["ManName"] ?></option>
							<?php
								endwhile;
							?>							  
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Giá:</label>
					<label for="txtMinPrice" class="col-sm-2 control-label">Tối thiểu:</label>
					<div class="col-sm-3">
						<input type="text" class="form-control" id="txtMinPrice" name="txtMinPrice" value='0'>
					</div>
					<label for="txtMaxPrice" class="col-sm-2 control-label">Tối đa:</label>
					<div class="col-sm-3">
						<input type="text" class="form-control" id="txtMaxPrice" name="txtMaxPrice" value='<?= $highestPrice ?>'>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button name="btnSearch" type="submit" class="btn btn-success">
							<!--<span class="glyphicon glyphicon-ok"></span>-->
							Tìm kiếm
						</button>						
					</div>
				</div>

			</form>

		</div>
	</div>

<?php
	if(isset($_GET["btnSearch"])) :
		$searchName = $_GET['txtName'];
		$searchCatID = $_GET['selCategories'];
		$searchManID = $_GET['selManufacturers'];
		$searchMinPrice = $_GET['txtMinPrice'];
		$searchMaxPrice = $_GET['txtMaxPrice'];
		$conditionString = " where ProName like '%$searchName%'"; 
		if($searchCatID != 0)
		{
			$conditionString .= " and CatID=$searchCatID";
		}
		if($searchManID != 0)
		{
			$conditionString .= " and ManID=$searchManID";
		}
		$conditionString .= " and Price >= $searchMinPrice and Price <= $searchMaxPrice";
		
		//Pagination
		
		$limit = 6;

		$current_page = 1;

			//Query string
			$queryString = $_SERVER['QUERY_STRING'];

			if (isset($_GET["page"])) {
				$current_page = $_GET["page"];
				$queryString = str_replace("&page=$current_page", "", $queryString);
			}
		
		$next_page = $current_page + 1;
		$prev_page = $current_page - 1;

		$c_sql = "select count(*) as num_rows from products" . $conditionString;
		$c_rs = load($c_sql);
		$c_row = $c_rs->fetch_assoc();
		$num_rows = $c_row["num_rows"];
		$num_pages = ceil($num_rows / $limit);

		if ($current_page < 1 || $current_page > $num_pages) {
			$current_page = 1;
		}

		$offset = ($current_page - 1) * $limit;

		//Final sql string

		$sql = "select * from products" . $conditionString . " limit $offset, $limit";
?>

	<div class="col-md-12">
		<div class="panel panel-primary">
		  	<div class="panel-heading">
		    	<h3 class="panel-title">Sản phẩm được tìm thấy</h3>
		  	</div>
		  	<div class="panel-body">
		    	<div class="row">
		    		<?php
						$rs = load($sql);
						
						$hasResults = false;
						
						if ($rs->num_rows > 0) :
							$hasResults = true;
							while ($row = $rs->fetch_assoc()) :
	    			?>

	    			<div class="col-sm-6 col-md-4">
						<div class="thumbnail">
							<img src="imgs/sp/<?= $row["ProID"] ?>/main.jpg" alt="...">
							<div class="caption">
								<h5 style="height: 40px;"><?= $row["ProName"] ?></h5>
								<h4><?= number_format($row["Price"]) ?></h4>
								<div>Xuất xứ: <?= $row["ProSource"] ?></div>
								<div>Số lượng: <?= $row["Quantity"] ?></div>
								<br>
								<p>
									<a href="productDetails.php?id=<?= $row["ProID"] ?>" class="btn btn-primary" role="button">Chi tiết</a>
									<a class="btn btn-danger add-one-to-cart" role="button" data-id="<?= $row["ProID"] ?>">
										<span class="glyphicon glyphicon-shopping-cart"></span>
										Đặt mua
									</a>
								</p>
							</div>
						</div>
					</div>

					<?php
							endwhile;
						else :
					?>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						Không có sản phẩm nào thỏa điều kiện.
					</div>
					<?php
						endif;
					?>
		    	</div>
		  	</div>
		</div>
	</div>

	<?php
		if($hasResults) :
	?>
		<!--Pagination-->
	<div class="col-sm-12 col-md-12">	
		<ul class="pagination">
			<li class="<?php if($current_page == 1) echo 'disabled' ?>">
				<a href="?<?= $queryString ?>">
					<span>Đầu</span>
				</a>
			</li>
			<li class="<?php if($current_page == 1) echo 'disabled' ?>">
				<a href="?<?= $queryString . "&page=$prev_page" ?>">
					<span>Trước</span>
				</a>
			</li>
			<?php for ($i = 1; $i <= $num_pages; $i++) : ?>
				<li class="<?php if ($i == $current_page) echo 'active' ?>">
					<a href="?<?= $queryString . "&page=$i" ?>"><?= $i ?></a>
				</li>
			<?php endfor; ?>

			<!-- <li class="active">
				<a href="#">
					1
					<span class="sr-only">(current)</span>
				</a>
			</li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li> -->
			<li class="<?php if($current_page == $num_pages) echo 'disabled' ?>">
				<a href="?<?= $queryString . "&page=$next_page" ?>">
					<span>Sau</span>
				</a>
			</li>
			<li class="<?php if($current_page == $num_pages) echo 'disabled' ?>">
				<a href="?<?= $queryString . "&page=$num_pages" ?>">
					<span>Cuối</span>
				</a>
			</li>
		</ul>
	</div>
	<!--Pagination-->

<?php
		endif;
	endif;
?>

</div>

<script src="assets/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>

<script type="text/javascript">
	$(function () {
		$('#txtMinPrice').TouchSpin({
			min: 0,
			max: <?= $highestPrice ?>,
			step: 50000
			// decimals: 0,
			// boostat: 5,
			// maxboostedstep: 10,
			// postfix: '%'
		});
		$('#txtMaxPrice').TouchSpin({
			min: 0,
			max: <?= $highestPrice ?>,
			step: 50000
			// decimals: 0,
			// boostat: 5,
			// maxboostedstep: 10,
			// postfix: '%'
		});
	});
</script>

<form id="f" method="post" action="apis/addItemToCart.php">
	<input type="hidden" id="txtProID" name="txtProID">
	<input type="hidden" id="txtQuantity" name="txtQuantity">
</form>

<script type="text/javascript">
	$('.add-one-to-cart').on('click', function() {
		var id = $(this).data('id');
		$('#txtProID').val(id);
	    $('#txtQuantity').val('1');
	    $('#f').submit();
	});
</script>