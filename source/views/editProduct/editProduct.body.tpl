<?php
	if($editFailure) :
?>
<div id="add-failure" class="alert alert-danger" role="alert">Đã xảy ra lỗi khi thêm sản phẩm.</div>
<?php
	endif;
?>
<?php
	$id = $_GET["id"];
	$sql = "select * from products where ProID = $id";
	$rs = load($sql);
	if ($rs->num_rows > 0) :
		$row = $rs->fetch_assoc();
?>
<div id="invalid-name" style="display:none;" class="alert alert-danger" role="alert">Tên sản phẩm trống.</div>
<div id="invalid-source" style="display:none;" class="alert alert-danger" role="alert">Xuất xứ trống.</div>
<div id="invalid-des" style="display:none;" class="alert alert-danger" role="alert">Mô tả trống.</div>
<div id="invalid-cat" style="display:none;" class="alert alert-danger" role="alert">Loại sản phẩm không hợp lệ.</div>
<div id="invalid-man" style="display:none;" class="alert alert-danger" role="alert">Nhà sản xuất không hợp lệ.</div>
<div id="product-existed" style="display:none;" class="alert alert-danger" role="alert">Sản phẩm cùng tên đã tồn tại.</div>

<form id="f" class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
	<input type="hidden" id="txtID" name="txtID" value="<?= $id ?>">
	<div class="form-group">
		<label class="col-sm-2 control-label">Sản phẩm</label>
		<div class="col-sm-8">
			<img src="imgs/sp/<?= $id ?>/main.jpg">
		</div>
	</div>
	<div class="form-group">
		<label for="txtName" class="col-sm-2 control-label">Tên sản phẩm</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" id="txtName" name="txtName" value="<?= $row["ProName"] ?>">
		</div>
	</div>
	<div class="form-group">
		<label for="txtPrice" class="col-sm-2 control-label">Giá</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" id="txtPrice" name="txtPrice" value="<?= $row["Price"] ?>">
		</div>
	</div>
	<div class="form-group">
		<label for="txtQuantity" class="col-sm-2 control-label">Số lượng</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" id="txtQuantity" name="txtQuantity" value="<?= $row["Quantity"] ?>">
		</div>
	</div>
	<div class="form-group">
		<label for="txtSource" class="col-sm-2 control-label">Xuất xứ</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" id="txtSource" name="txtSource" value="<?= $row["ProSource"] ?>">
		</div>
	</div>
	<div class="form-group">
		<label for="txtDes" class="col-sm-2 control-label">Mô tả</label>
		<div class="col-sm-8">
			<textarea rows="6" id="txtDes" name="txtDes" class="form-control"></textarea>
		</div>
	</div>
	<div class="form-group">
		<label for="selCategory" class="col-sm-2 control-label">Loại sản phẩm</label>
		<div class="col-sm-8">
			<select id="selCategory" name="selCategory" class="form-control">
			<?php
				$sql2 = "select * from categories";
				$rs2 = load($sql2);
				$selected = false;
				while ($row2 = $rs2->fetch_assoc()) :
			?>
				<option value="<?= $row2["CatID"] ?>" <?php if($row2["CatID"] == $row["CatID"]) { $selected = true; echo "selected"; } ?>><?= $row2["CatName"] ?></option>
			<?php
				endwhile;
			?>
			<?php
				if($selected == false) :
			?>
				<option value="-1" selected>(không tìm thấy)</option>
			<?php
				endif;
			?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="selManufacturer" class="col-sm-2 control-label">Nhà sản xuất</label>
		<div class="col-sm-8">
			<select id="selManufacturer" name="selManufacturer" class="form-control">
			<?php
				$sql3 = "select * from manufacturers";
				$rs3 = load($sql3);
				$selected = false;
				while ($row3 = $rs3->fetch_assoc()) :
			?>
				<option value="<?= $row3["ManID"] ?>" <?php if($row3["ManID"] == $row["ManID"]) { $selected = true; echo "selected"; } ?>><?= $row3["ManName"] ?></option>
			<?php
				endwhile;
			?>	
			<?php
				if($selected == false) :
			?>
				<option value="-1" selected>(không tìm thấy)</option>
			<?php
				endif;
			?>	
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="fuImage" class="col-sm-2 control-label">Ảnh sản phẩm</label>
		<div class="col-sm-8">
			<input type="file" class="form-control" id="fuImage" name="fuImage">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<a href="viewProductDetails.php?id=<?= $id ?>" class="btn btn-default">
				<span class="glyphicon glyphicon-chevron-left"></span>
				Quay lại
			</a>
			<button name="btnEdit" type="submit" class="btn btn-success">
				<span class="glyphicon glyphicon-ok"></span>
				Cập nhật
			</button>
		</div>
	</div>
</form>
<script src="assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	tinymce.init({
	    selector: '#txtDes',
	    menubar: false,
	    toolbar1: "styleselect | bold italic | link image | alignleft aligncenter alignright | bullist numlist | fontselect | fontsizeselect | forecolor backcolor",
	    // toolbar2: "",
	    // plugins: 'link image textcolor',
	    //height: 300,
	    // encoding: "xml",
	    init_instance_callback: "setDesContent"
	});
	function setDesContent()
	{		
		tinymce.activeEditor.setContent(`<?= $row["Des"] ?>`);
	}	
</script>
<script src="assets/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
<script type="text/javascript">
	$(function () {
		$('#txtPrice').TouchSpin({
			min: 50000,
			max: 100000000,
			step: 50000
			// decimals: 0,
			// boostat: 5,
			// maxboostedstep: 10,
			// postfix: '%'
		});
		$('#txtQuantity').TouchSpin({
			min: 1,
			max: 100
			// step: 1,
			// decimals: 0,
			// boostat: 5,
			// maxboostedstep: 10,
			// postfix: '%'
		});
	});
</script>
<script type="text/javascript">
	$('#f').on('submit', function (e) {
		e.preventDefault();

		var invalid = false;
		var form = this;

		var name = $('#txtName').val();
		if (name.length == 0)
		{
			$("#invalid-name").show();
			invalid = true;
		}
		else
		{
			$("#invalid-name").hide();	
		}
		
		var source = $("#txtSource").val();
		var sourcePattern = /^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]+$/;
		if (source.length == 0 || !sourcePattern.test(source)) {
			$("#invalid-source").show();
			invalid = true;
		}
		else
		{
			$("#invalid-source").hide();	
		}

		var des = tinymce.activeEditor.getBody();
		if (des.length == 0) {
			$("#invalid-des").show();
			invalid = true;
		}
		else
		{
			$("#invalid-des").hide();	
		}

		var catId = $("#selCategory").val();
		if(catId == -1)
		{
			$("#invalid-cat").show();
			invalid = true;
		}
		else
		{
			$("#invalid-cat").hide();	
		}

		var manId = $("#selManufacturer").val();
		if(manId == -1)
		{
			$("#invalid-man").show();
			invalid = true;
		}
		else
		{
			$("#invalid-man").hide();	
		}

		var url = 'apis/checkProduct.php?product=' + name + "&id=<?= $id ?>";
		$.getJSON(url, function (data) {
			if (data == 1) {				
				$('#product-existed').show();
			} else {
				$('#product-existed').hide();
				if(invalid == false)
				{
					form.submit();
				}
			}
		});
	});
</script>

<?php
	else :
?>
<div class="col-md-12">
	Không có sản phẩm thoả điều kiện.
</div>
<?php
	endif;
?>