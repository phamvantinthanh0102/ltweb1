<div class="row">
	<?php
		$id = $_GET["id"];
		$sql = "select * from products where ProID = $id";
		$rs = load($sql);
		if ($rs->num_rows > 0) :
			$row = $rs->fetch_assoc();

			$catId = $row["CatID"];
			$catName = "(không tìm thấy)";
			$sql2 = "select * from categories where CatID = $catId";
			$rs2 = load($sql2);
			if($rs2->num_rows > 0)
			{
				$row2 = $rs2->fetch_assoc();
				$catName = $row2["CatName"];
			}

			$manId = $row["ManID"];
			$manName = "(không tìm thấy)";
			$sql3 = "select * from manufacturers where ManID = $manId";
			$rs3 = load($sql3);
			if($rs3->num_rows > 0)
			{
				$row3 = $rs3->fetch_assoc();
				$manName = $row3["ManName"];
			}

			$quantityMax = $row["Quantity"];
			if(array_key_exists($id, $_SESSION["cart"]))
			{
				$quantityMax -= $_SESSION["cart"][$id];
			}
			
	?>
	<div class="col-md-12">
		<img src="imgs/sp/<?= $row["ProID"] ?>/main.jpg">
	</div>
	<div class="col-md-12">
		<h2><?= $row["ProID"] . ". " . $row["ProName"] ?></h2>
	</div>
	<div class="col-md-12">
		<h3 style="margin-top:20px; margin-bottom:0;">Giá: <?= number_format($row["Price"]) ?> VNĐ</h3>
		<h3 style="margin-top:0; margin-bottom:0;">Số lượng: <?= $row["Quantity"] ?></h3>
	</div>
	<div class="col-md-12">
		<h3 style="margin-top:15px; margin-bottom:0;">Số lượng xem: <?= $row["ViewCount"] ?> lượt</h3>
		<h3 style="margin-top:0; margin-bottom:0;">Số lượng bán: <?= $row["SellCount"] ?> lượt</h3>
	</div>
	<div class="col-md-12">
		<h3 style="margin-top:15px; margin-bottom:0;">Mô tả:</h3><br>
		<?= $row["Des"] ?>
	</div>
	<div class="col-md-12">
		<h3 style="margin-top:15px; margin-bottom:0;">Xuất xứ: <?= $row["ProSource"] ?></h3>
		<h3 style="margin-top:15px; margin-bottom:0;">Loại sản phẩm: <?= $catName ?></h3>
		<h3 style="margin-top:15px; margin-bottom:20px;">Nhà sản xuất: <?= $manName ?></h3>
	</div>

	<div class="col-md-12">
		<form method="post" action="apis/addItemToCart.php">
			<div class="input-group">
				<input type="hidden" name="txtProID" value="<?= $_GET["id"] ?>">
			</div>
			<div class="col-md-4">
				<input type="text" class="form-control" value="<?php if($quantityMax == 0) echo "0"; else echo "1"; ?>" name="txtQuantity" id="txtQuantity">
			</div>
			<div class="col-md-4">
				<span class="input-group-btn">
					<button class="btn btn-success" type="submit" name="btnAddItemToCart">
						Thêm sản phẩm vào giỏ hàng
						<span class="glyphicon glyphicon-plus"></span>
					</button>
				</span>
			</div>
		</form>
	</div>
	
	<div class="col-md-12">
		<br>
		<br>
	</div>

	<div class="col-md-12">
		<div class="panel panel-info">
		  	<div class="panel-heading">
		    	<h3 class="panel-title">Sản phẩm cùng loại</h3>
		  	</div>
		  	<div class="panel-body">
		    	<div class="row">
		    		<?php
						//$sql = "select CatID from products where ProID = $id";
						//$rs = load($sql);
						//$row = $rs->fetch_assoc();
						//$catId = $row["CatID"];

						$sql = "select * from products where CatID = $catId and not proID = $id order by rand() limit 5";
						$rs = load($sql);
						
						if ($rs->num_rows > 0) :

							while ($row = $rs->fetch_assoc()) :
	    			?>

	    			<div class="col-sm-6 col-md-4">
						<div class="thumbnail">
							<img src="imgs/sp/<?= $row["ProID"] ?>/main.jpg" alt="...">
							<div class="caption">
								<h5 style="height: 40px;"><?= $row["ProName"] ?></h5>
								<h4><?= number_format($row["Price"]) ?></h4>
								<div>Xuất xứ: <?= $row["ProSource"] ?></div>
								<div>Số lượng: <?= $row["Quantity"] ?></div>
								<br>
								<p>
									<a href="productDetails.php?id=<?= $row["ProID"] ?>" class="btn btn-primary" role="button">Chi tiết</a>
									<a href="#" class="btn btn-danger add-one-to-cart" role="button" data-id="<?= $row["ProID"] ?>">
										<span class="glyphicon glyphicon-shopping-cart"></span>
										Đặt mua
									</a>
								</p>
							</div>
						</div>
					</div>

					<?php
							endwhile;
						else :
					?>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						Không có sản phẩm cùng loại nào khác.
					</div>
					<?php
						endif;
					?>
		    	</div>
		  	</div>
		</div>
	</div>

	<div class="col-md-12">
		<div class="panel panel-info">
		  	<div class="panel-heading">
		    	<h3 class="panel-title">Sản phẩm cùng nhà sản xuất</h3>
		  	</div>
		  	<div class="panel-body">
		    	<div class="row">
		    		<?php
						//$sql = "select ManID from products where ProID = $id";
						//$rs = load($sql);
						//$row = $rs->fetch_assoc();
						//$manId = $row["ManID"];

						$sql = "select * from products where ManID = $manId and not proID = $id order by rand() limit 5";
						$rs = load($sql);
						
						if ($rs->num_rows > 0) :

							while ($row = $rs->fetch_assoc()) :
	    			?>

	    			<div class="col-sm-6 col-md-4">
						<div class="thumbnail">
							<img src="imgs/sp/<?= $row["ProID"] ?>/main.jpg" alt="...">
							<div class="caption">
								<h5 style="height: 40px;"><?= $row["ProName"] ?></h5>
								<h4><?= number_format($row["Price"]) ?></h4>
								<div>Xuất xứ: <?= $row["ProSource"] ?></div>
								<div>Số lượng: <?= $row["Quantity"] ?></div>
								<br>
								<p>
									<a href="productDetails.php?id=<?= $row["ProID"] ?>" class="btn btn-primary" role="button">Chi tiết</a>
									<a href="#" class="btn btn-danger add-one-to-cart" role="button" data-id="<?= $row["ProID"] ?>">
										<span class="glyphicon glyphicon-shopping-cart"></span>
										Đặt mua
									</a>
								</p>
							</div>
						</div>
					</div>

					<?php
							endwhile;
						else :
					?>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						Không có sản phẩm cùng nhà sản xuất nào khác.
					</div>
					<?php
						endif;
					?>
		    	</div>
		  	</div>
		</div>
	</div>
<script src="assets/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
<script type="text/javascript">
	$(function () {
		$('#txtQuantity').TouchSpin({
			min: <?php if($quantityMax == 0) echo "0"; else echo "1"; ?>,
			max: <?= $quantityMax ?>
			// step: 1,
			// decimals: 0,
			// boostat: 5,
			// maxboostedstep: 10,
			// postfix: '%'
		});
	});
    $.post("apis/viewCount.php",
    {
        txtViewProID: "<?= $id ?>"
    });
</script>
<?php
	else :
?>
	<div class="col-md-12">
		Không có sản phẩm thoả điều kiện.
	</div>
</div>
<?php
	endif;
?>
<form id="f" method="post" action="apis/addItemToCart.php">
	<input type="hidden" id="txtProIDOne" name="txtProID">
	<input type="hidden" id="txtQuantityOne" name="txtQuantity">
</form>
<script type="text/javascript">
	$('.add-one-to-cart').on('click', function() {
		var id = $(this).data('id');
		$('#txtProIDOne').val(id);
	    $('#txtQuantityOne').val('1');
	    $('#f').submit();
	});
</script>