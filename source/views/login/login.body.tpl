<div class="row">
	<?php
		if(!isset($_SESSION["current_user"])) :
			if($loginFailed == 1) :
	?>
	<div class="col-md-12">
		<div class="panel panel-danger">
		  	<div class="panel-heading">
		    	<h3 class="panel-title">Đăng nhập thất bại</h3>
		  	</div>
		  	<div class="panel-body">
		    	<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						Tên đăng nhập hoặc mật khẩu không chính xác.
					</div>
		    	</div>
		  	</div>
		</div>
	</div>
	<?php
		endif;
	?>
	<div class="col-md-12">
		<div id="invalid-username" style="display:none;" class="alert alert-danger" role="alert">Tên đăng nhập không hợp lệ.</div>
	</div>
	<div class="col-md-12">
		<div id="invalid-password" style="display:none;" class="alert alert-danger" role="alert">Mật khẩu không hợp lệ.</div>
	</div>
	<div class="col-md-6 col-md-offset-3">
		<form id="f" method="post" action="">
			<div class="form-group">
				<label for="txtUserName">Tên đăng nhập</label>
				<input type="text" class="form-control" id="txtUserName" name="txtUserName" placeholder="">
			</div>
			<div class="form-group">
				<label for="txtPassword">Mật khẩu</label>
				<input type="password" class="form-control" id="txtPassword" name="txtPassword" placeholder="">
			</div>
			<div class="checkbox">
				<label>
					<input name="cbRemember" type="checkbox"> Ghi nhớ
				</label>
			</div>
			<button type="submit" class="btn btn-success btn-block" name="btnLogin">
				<span class="glyphicon glyphicon-user"></span>
				Đăng nhập
			</button>
		</form>
	</div>
	<?php
		else :
	?>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		Bạn đã đăng nhập dưới tên tài khoản <b><?= $_SESSION["current_user"]->f_Username ?></b>.
	</div>
	<?php
		endif;
	?>
</div>

<script type="text/javascript">
	$('#f').on('submit', function (e) {
		e.preventDefault();

		var invalid = false;
		var form = this;

		var username = $('#txtUserName').val();
		var usernamePattern = /^[a-zA-Z][a-zA-Z0-9]*$/;
		if (username.length == 0 || usernamePattern.test(username) == false) {
			$("#invalid-username").show();
			invalid = true;
		}
		else
		{
			$("#invalid-username").hide();	
		}

		var passwordPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{0,}$/;
		var password = $('#txtPassword').val();
		if (password.length == 0 || passwordPattern.test(password) == false) {
			$("#invalid-password").show();
			invalid = true;
		}
		else
		{
			$("#invalid-password").hide();	
		}

		if(invalid == false)
		{
			form.submit();
		}
	});
</script>