<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
  			<div class="panel-heading">
   				<h3 class="panel-title">Quản lí sản phẩm</h3>
  			</div>
	  		<div class="panel-body">
	   			<div class="col-md-3">
					<a class="btn btn-primary btn-block" href="addProduct.php" role="button">
						<span class="glyphicon glyphicon-th-large"></span>
						Thêm sản phẩm
					</a>
				</div>
				<div class="col-md-3">
					<a class="btn btn-primary btn-block" href="viewProducts.php" role="button">
						<span class="glyphicon glyphicon-th-large"></span>
						Xóa, sửa sản phẩm
					</a>
				</div>
	  		</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="panel panel-success">
  			<div class="panel-heading">
   				<h3 class="panel-title">Quản lí loại sản phẩm</h3>
  			</div>
	  		<div class="panel-body">
	   			<div class="col-md-3">
					<a class="btn btn-success btn-block" href="addCategory.php" role="button">
						<span class="glyphicon glyphicon-th-large"></span>
						Thêm loại sản phẩm
					</a>
				</div>
				<div class="col-md-3">
					<a class="btn btn-success btn-block" href="editDeleteCategory.php" role="button">
						<span class="glyphicon glyphicon-th-large"></span>
						Xóa, sửa loại sản phẩm
					</a>
				</div>
	  		</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="panel panel-info">
  			<div class="panel-heading">
   				<h3 class="panel-title">Quản lí nhà sản xuất</h3>
  			</div>
	  		<div class="panel-body">
	   			<div class="col-md-3">
					<a class="btn btn-info btn-block" href="addManufacturer.php" role="button">
						<span class="glyphicon glyphicon-th-large"></span>
						Thêm nhà sản xuất
					</a>
				</div>
				<div class="col-md-3">
					<a class="btn btn-info btn-block" href="editDeleteManufacturer.php" role="button">
						<span class="glyphicon glyphicon-th-large"></span>
						Xóa, sửa nhà sản xuất
					</a>
				</div>
	  		</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="panel panel-warning">
  			<div class="panel-heading">
   				<h3 class="panel-title">Quản lí đơn hàng</h3>
  			</div>
	  		<div class="panel-body">
	   			<div class="col-md-4">
					<a class="btn btn-warning btn-block" href="viewOrders.php" role="button">
						<span class="glyphicon glyphicon-th-large"></span>
						Cập nhật trạng thái đơn hàng
					</a>
				</div>
	  		</div>
		</div>
	</div>
</div>