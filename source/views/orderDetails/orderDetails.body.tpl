<div class="row">
	<?php
		$sql = "select * from orderstatus";
		$rs = load($sql);
		$statusDescription = array();
		$statusColor = array();
		while($row = $rs->fetch_assoc())
		{
			$statusDescription[$row["StatusID"]] = $row["StatusDescription"];
			$statusColor[$row["StatusID"]] = $row["StatusColor"];
		}

		$orderId = $_GET["id"];
		$sql = "select UserID, StatusID from orders where OrderID = $orderId";
		$rs = load($sql);
		$row = $rs->fetch_assoc();
		if($rs->num_rows > 0 && ($row["UserID"] == $_SESSION["current_user"]->f_ID || $_SESSION["current_user"]->f_Permission == 1)) :
			$sql2 = "select * from orderdetails where OrderID = $orderId";
			$rs2 = load($sql2);
	?>
	<div class="col-md-12">
		<table class="table table-hover">
			<thead>
				<tr>
					<th class="col-md-2">Mã sản phẩm</th>
					<th class="col-md-4">Sản phẩm</th>
					<th class="col-md-2">Số lượng</th>
					<th class="col-md-2">Giá</th>
					<th class="col-md-2">Thành tiền</th>
				</tr>
			</thead>
			<tbody>
			<?php
				while ($row2 = $rs2->fetch_assoc()) :
					$proId = $row2["ProID"];
					$sql3 = "Select ProName from products where ProID = '$proId'";
					$rs3 = load($sql3);
					$row3 = "";
					if($rs3->num_rows > 0)
					{
						$row3 = $rs3->fetch_assoc();
					}
					else
					{
						$row3["ProName"] = "(không tìm thấy)";
					}
			?>
				<tr>
					<td><?= $proId ?></td>
					<td><?= $row3["ProName"] ?></td>
					<td><?= $row2["Quantity"] ?></td>
					<td><?= number_format($row2["Price"]) ?></td>
					<td><?= number_format($row2["Amount"]) ?></td>
				</tr>
			<?php
				endwhile;
			?>
			</tbody>
		</table>
	</div>
</div>
<div class="row">
	<div class="col-md-4">
		<h3><span class="label label-default" style="color:grey; background-color:<?= $statusColor[$row["StatusID"]] ?>;">Tình trạng đơn hàng: <?= $statusDescription[$row["StatusID"]] ?></span></h3>
	</div>
</div>
<div class="row">
	<div class="col-md-4">
		<a href="viewOrders.php" class="btn btn-default">
			<span class="glyphicon glyphicon-chevron-left"></span>
			Quay lại
		</a>
	</div>
</div>
	<?php
		else :
	?>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		Mã hóa đơn không hợp lệ.
	</div>
	<?php
		endif;
	?>
</div>