<div class="row">
	<?php
		$limit = 6;

		$current_page = 1;
		if (isset($_GET["page"])) {
			$current_page = $_GET["page"];
		}
		
		$next_page = $current_page + 1;
		$prev_page = $current_page - 1;

		$id = $_GET["id"];
		$c_sql = "select count(*) as num_rows from products where CatID = $id";
		$c_rs = load($c_sql);
		$c_row = $c_rs->fetch_assoc();
		$num_rows = $c_row["num_rows"];
		$num_pages = ceil($num_rows / $limit);

		if ($current_page < 1 || $current_page > $num_pages) {
			$current_page = 1;
		}

		$offset = ($current_page - 1) * $limit;

		$sql = "select * from products where CatID = $id limit $offset, $limit";
		$rs = load($sql);

		if ($rs->num_rows > 0) :
	?>
	
	<div class="col-sm-12 col-md-12">
		
	<?php
			while ($row = $rs->fetch_assoc()) :
	?>
	<div class="col-sm-6 col-md-4">
		<div class="thumbnail">
			<img src="imgs/sp/<?= $row["ProID"] ?>/main.jpg" alt="...">
			<div class="caption">
				<h5 style="height: 40px;"><?= $row["ProName"] ?></h5>
				<h4><?= number_format($row["Price"]) ?></h4>
				<div>Xuất xứ: <?= $row["ProSource"] ?></div>
				<div>Số lượng: <?= $row["Quantity"] ?></div>
				<br>
				<p>
					<a href="productDetails.php?id=<?= $row["ProID"] ?>" class="btn btn-primary" role="button">Chi tiết</a>
					<a href="#" class="btn btn-danger add-one-to-cart" role="button" data-id="<?= $row["ProID"] ?>">
						<span class="glyphicon glyphicon-shopping-cart"></span>
						Đặt mua
					</a>
				</p>
			</div>
		</div>
	</div>
	
	<?php
			endwhile;
	?>

	</div>

	<!--Pagination-->
	<div class="col-sm-12 col-md-12">	
		<ul class="pagination">
			<li class="<?php if($current_page == 1) echo 'disabled' ?>">
				<a href="?id=<?= $id ?>">
					<span>Đầu</span>
				</a>
			</li>
			<li class="<?php if($current_page == 1) echo 'disabled' ?>">
				<a href="?id=<?= $id ?>&page=<?= $prev_page ?>">
					<span>Trước</span>
				</a>
			</li>
			<?php for ($i = 1; $i <= $num_pages; $i++) : ?>
				<li class="<?php if ($i == $current_page) echo 'active' ?>">
					<a href="?id=<?= $id ?>&page=<?= $i ?>"><?= $i ?></a>
				</li>
			<?php endfor; ?>

			<!-- <li class="active">
				<a href="#">
					1
					<span class="sr-only">(current)</span>
				</a>
			</li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li> -->
			<li class="<?php if($current_page == $num_pages) echo 'disabled' ?>">
				<a href="?id=<?= $id ?>&page=<?= $next_page ?>">
					<span>Sau</span>
				</a>
			</li>
			<li class="<?php if($current_page == $num_pages) echo 'disabled' ?>">
				<a href="?id=<?= $id ?>&page=<?= $num_pages ?>">
					<span>Cuối</span>
				</a>
			</li>
		</ul>
	</div>
	<!--Pagination-->

	<?php
		else :
	?>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		Không có sản phẩm thoả điều kiện.
	</div>
	<?php
		endif;
	?>
</div>
<form id="f" method="post" action="apis/addItemToCart.php">
	<input type="hidden" id="txtProID" name="txtProID">
	<input type="hidden" id="txtQuantity" name="txtQuantity">
</form>
<script type="text/javascript">
	$('.add-one-to-cart').on('click', function() {
		var id = $(this).data('id');
		$('#txtProID').val(id);
	    $('#txtQuantity').val('1');
	    $('#f').submit();
	});
</script>