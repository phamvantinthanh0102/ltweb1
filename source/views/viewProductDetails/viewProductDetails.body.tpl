<div class="row">
	<?php
		$id = $_GET["id"];
		$sql = "select * from products where ProID = $id";
		$rs = load($sql);
		if ($rs->num_rows > 0) :
			$row = $rs->fetch_assoc();

			$catId = $row["CatID"];
			$catName = "(không tìm thấy)";
			$sql2 = "select * from categories where CatID = $catId";
			$rs2 = load($sql2);
			if($rs2->num_rows > 0)
			{
				$row2 = $rs2->fetch_assoc();
				$catName = $row2["CatName"];
			}

			$manId = $row["ManID"];
			$manName = "(không tìm thấy)";
			$sql3 = "select * from manufacturers where ManID = $manId";
			$rs3 = load($sql3);
			if($rs3->num_rows > 0)
			{
				$row3 = $rs3->fetch_assoc();
				$manName = $row3["ManName"];
			}

			$productDate = strtotime($row["StartDate"]);
	?>
	<div class="col-md-12">
		<img src="imgs/sp/<?= $row["ProID"] ?>/main.jpg">
	</div>
	<div class="col-md-12">
		<h2><?= $row["ProID"] . ". " . $row["ProName"] ?></h2>
	</div>
	<div class="col-md-12">
		<h3 style="margin-top:20px; margin-bottom:0;">Giá: <?= number_format($row["Price"]) ?> VNĐ</h3>
		<h3 style="margin-top:0; margin-bottom:0;">Số lượng: <?= $row["Quantity"] ?></h3>
	</div>
	<div class="col-md-12">
		<h3 style="margin-top:15px; margin-bottom:0;">Ngày tạo: <?= date("j/n/Y", $productDate) ?></h3>
	</div>
	<div class="col-md-12">
		<h3 style="margin-top:15px; margin-bottom:0;">Số lượng xem: <?= $row["ViewCount"] ?> lượt</h3>
		<h3 style="margin-top:0; margin-bottom:0;">Số lượng bán: <?= $row["SellCount"] ?> lượt</h3>
	</div>
	<div class="col-md-12">
		<h3 style="margin-top:15px; margin-bottom:0;">Mô tả:</h3><br>
		<?= $row["Des"] ?>
	</div>
	<div class="col-md-12">
		<h3 style="margin-top:15px; margin-bottom:0;">Xuất xứ: <?= $row["ProSource"] ?></h3>
		<h3 style="margin-top:15px; margin-bottom:0;">Loại sản phẩm: <?= $catName ?></h3>
		<h3 style="margin-top:15px; margin-bottom:20px;">Nhà sản xuất: <?= $manName ?></h3>
	</div>

	<div class="col-md-12">
		<a class="btn btn-success" href="editProduct.php?id=<?= $id ?>">
			Chỉnh sửa
			<span class="glyphicon glyphicon-pencil"></span>			
		</a>
		<a id="delete-button" class="btn btn-danger">
			Xóa sản phẩm
			<span class="glyphicon glyphicon-remove"></span>
		</a>
	</div>
	
	<div class="col-md-12">
		<br>
	</div>

	<div class="col-md-12">
		<a href="viewProducts.php" class="btn btn-default">
			<span class="glyphicon glyphicon-chevron-left"></span>
			Quay lại
		</a>
	</div>

	<form id="delete-form" method="post" action="apis/deleteProduct.php">
		<input type="hidden" name="txtDeleteProID" value="<?= $_GET["id"] ?>">
	</form>

	<?php
		else :
	?>
	<div class="col-md-12">
		Không có sản phẩm thoả điều kiện.
	</div>
	<?php
		endif;
	?>
</div>

<script type="text/javascript">
	$('#delete-button').on('click', function() {		
	    $('#delete-modal').modal("show");
	});
</script>

<div class="modal fade" id="delete-modal" role="dialog">
    <div class="modal-dialog">
    
    	<!-- Modal content-->
    	<div class="modal-content">
        	<div class="modal-header">
          		<button type="button" class="close" data-dismiss="modal">&times;</button>
          		<h4 class="modal-title">Xóa sản phẩm</h4>
        	</div>
        <div class="modal-body">
          	<p>Bạn có chắc muốn xóa sản phẩm này?</p>
        </div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-default" id="modal-yes-button">Có</button>
          	<button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
        </div>
      </div>
      
    </div>
</div>

<script type="text/javascript">
	$("#modal-yes-button").on('click', function() {
		$('#delete-form').submit();
	});
</script>