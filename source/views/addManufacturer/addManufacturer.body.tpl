<div class="row">
<?php
	if(isset($_POST["btnAddManufacturer"])) :
		$manufacturerName = $_POST["txtManufacturer"];
		$sql = "insert into manufacturers(ManName) values('$manufacturerName')";
		$rs = load($sql);
		if($rs == true) :
?>
	<div class="col-md-12">
		<div class="alert alert-info" role="alert"><b>Chúc mừng!</b> Bạn đã thêm thành công nhà sản xuất <b><?= $manufacturerName ?></b>!</div>
	</div>
<?php
		else :
?>
	<div class="col-md-12">
		<div class="alert alert-danger" role="alert"><b>Lỗi!</b> Quá trình thêm nhà sản xuất <b><?= $manufacturerName ?></b> đã thất bại!</div>
	</div>
<?php
		endif;
	endif;
?>
	<div class="col-md-6 col-md-offset-3">
		<form method="post" action="">
			<div class="form-group">
				<label for="txtManufacturer">Tên nhà sản xuất</label>
				<input type="text" class="form-control" id="txtManufacturer" name="txtManufacturer">
			</div>
			<button type="submit" class="btn btn-info btn-block" name="btnAddManufacturer">
				<span class="glyphicon glyphicon-ok"></span>
				Thêm nhà sản xuất
			</button>
			<a href="adminDashboard.php" class="btn btn-default btn-block">
				<span class="glyphicon glyphicon-chevron-left"></span>
				Quay lại
			</a>
		</form>
	</div>
</div>