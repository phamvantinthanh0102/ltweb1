
<?php
	if($editFailure) :
?>
<div id="edit-failure" class="alert alert-danger" role="alert">Đã xảy ra lỗi khi cập nhật tài khoản.</div>
<?php
	endif;
?>

<div id="invalid-password" style="display:none;" class="alert alert-danger" role="alert">Mật khẩu phải chứa ít nhất một ký tự thường, một ký tự hoa, và số.</div>
<div id="invalid-confirm-password" style="display:none;" class="alert alert-danger" role="alert">Mật khẩu xác nhận không khớp.</div>
<div id="invalid-name" style="display:none;" class="alert alert-danger" role="alert">Họ tên không hợp lệ.</div>
<div id="invalid-email" style="display:none;" class="alert alert-danger" role="alert">Email không hợp lệ.</div>
<div id="invalid-dob" style="display:none;" class="alert alert-danger" role="alert">Ngày sinh không hợp lệ.</div>
<div id="invalid-address" style="display:none;" class="alert alert-danger" role="alert">Địa chỉ trống.</div>
<div id="invalid-phone-number" style="display:none;" class="alert alert-danger" role="alert">Số điện thoại không hợp lệ.</div>

<form id="f" class="form-horizontal" method="POST" action="">
<!--
	<div class="form-group">
		<label for="txtPassword" class="col-sm-2 control-label">Mật khẩu</label>
		<div class="col-sm-8">
			<input type="password" class="form-control" id="txtPassword" name="txtPassword">
		</div>
	</div>
	<div class="form-group">
		<label for="txtConfirmPassword" class="col-sm-2 control-label">Nhập lại mật khẩu</label>
		<div class="col-sm-8">
			<input type="password" class="form-control" id="txtConfirmPassword" name="txtConfirmPassword">
		</div>
	</div>
-->
	<div class="form-group">
		<label for="txtName" class="col-sm-2 control-label">Họ tên</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" id="txtName" name="txtName" value="<?= $_SESSION["current_user"]->f_Name ?>">
		</div>
	</div>
	<div class="form-group">
		<label for="txtEmail" class="col-sm-2 control-label">Email</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" id="txtEmail" name="txtEmail" value="<?= $_SESSION["current_user"]->f_Email ?>">
		</div>
	</div>
	<div class="form-group">
		<label for="txtDOB" class="col-sm-2 control-label">Ngày sinh</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" id="txtDOB" name="txtDOB" value="<?= $_SESSION["current_user"]->f_DOB ?>">
		</div>
	</div>
	<input type="hidden" class="form-control" id="txtDOBFormatted" name="txtDOBFormatted">
	<div class="form-group">
		<label for="txtAddress" class="col-sm-2 control-label">Địa chỉ</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" id="txtAddress" name="txtAddress" value="<?= $_SESSION["current_user"]->f_Address ?>">
		</div>
	</div>
	<div class="form-group">
		<label for="txtPhoneNumber" class="col-sm-2 control-label">Số điện thoại</label>
		<div class="col-sm-8">
			<input type="text" class="form-control" id="txtPhoneNumber" name="txtPhoneNumber" value="<?= $_SESSION["current_user"]->f_PhoneNumber ?>">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<a href="profile.php" class="btn btn-default">
				<span class="glyphicon glyphicon-chevron-left"></span>
				Quay lại
			</a>
			<button name="btnEdit" type="submit" class="btn btn-success">
				<span class="glyphicon glyphicon-ok"></span>
				Cập nhật
			</button>
		</div>
	</div>
</form>
<script src="lib/MySQLDate.js"></script>
<script type="text/javascript">
	$('#f').on('submit', function (e) {
		$('#edit-failure').hide();
		e.preventDefault();

		var invalid = false;
		var form = this;

		/*var passwordPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{0,}$/;
		var password = $('#txtPassword').val();
		if (password.length == 0 || passwordPattern.test(password) == false) {
			$("#invalid-password").show();
			invalid = true;
		}
		else
		{
			$("#invalid-password").hide();	
		}

		var confirmPassword = $('#txtConfirmPassword').val();
		if (confirmPassword != password) {
			$("#invalid-confirm-password").show();
			invalid = true;
		}
		else
		{
			$("#invalid-confirm-password").hide();	
		}*/

		var namePattern = /^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]+$/;
		var name = $('#txtName').val();
		if (name.length == 0 || namePattern.test(name) == false) {
			$("#invalid-name").show();
			invalid = true;
		}
		else
		{
			$("#invalid-name").hide();	
		}

		var emailPattern = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)/;
		var email = $('#txtEmail').val();
		if (email.length == 0 || emailPattern.test(email) == false) {
			$("#invalid-email").show();
			invalid = true;
		}		
		else
		{
			$("#invalid-email").hide();	
		}

		var dob = $('#txtDOB').val();
		var checkDate = isNaN(Date.parse(dob));
		if(checkDate == true)
		{
			$("#invalid-dob").show();
			invalid = true;
		}
		else
		{
			var dobFormatted = new Date(dob);
			dobFormatted = toMySQLDateFormat(dobFormatted);
			$("#txtDOBFormatted").val(dobFormatted);
			$("#invalid-dob").hide();
		}

		var address = $('#txtAddress').val();
		if(address.length == 0)
		{
			$('#invalid-address').show();
			invalid = true;
		}
		else
		{
			$('#invalid-address').hide();
		}

		var phoneNumber = $('#txtPhoneNumber').val();
		var phoneNumberPattern = /^[0-9]*$/;
		if (phoneNumber.length == 0 || phoneNumberPattern.test(phoneNumber) == false) {
			$("#invalid-phone-number").show();
			invalid = true;
		}
		else
		{
			$("#invalid-phone-number").hide();	
		}

		if(invalid == false)
		{
			form.submit();
		}
	});
</script>