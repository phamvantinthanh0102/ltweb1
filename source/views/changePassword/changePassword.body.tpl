<?php
	if($changeFailure == -1) :
?>
<div id="change-failure" class="alert alert-success" role="alert"><b>Chúc mừng!</b> Bạn đã thay đổi thành công mật khẩu của mình.</div>
<?php
	endif;
?>

<?php
	if($changeFailure == 1) :
?>
<div id="change-failure" class="alert alert-danger" role="alert">Đã xảy ra lỗi khi thay đổi mật khẩu.</div>
<?php
	endif;
?>

<?php
	if($changeFailure == 2) :
?>
<div id="change-failure" class="alert alert-danger" role="alert">Mật khẩu cũ không trùng khớp.</div>
<?php
	endif;
?>

<div id="invalid-new-password" style="display:none;" class="alert alert-danger" role="alert">Mật khẩu mới phải chứa ít nhất một ký tự thường, một ký tự hoa, và số.</div>
<div id="invalid-confirm-password" style="display:none;" class="alert alert-danger" role="alert">Mật khẩu xác nhận không khớp.</div>
<div id="invalid-old-password" style="display:none;" class="alert alert-danger" role="alert">Mật khẩu cũ không hợp lệ. Mật khẩu phải chứa ít nhất một ký tự thường, một ký tự hoa, và số.</div>

<form id="f" class="form-horizontal" method="POST" action="">
	<div class="form-group">
		<label for="txtPassword" class="col-sm-2 control-label">Mật khẩu</label>
		<div class="col-sm-8">
			<input type="password" class="form-control" id="txtNewPassword" name="txtNewPassword">
		</div>
	</div>
	<div class="form-group">
		<label for="txtConfirmPassword" class="col-sm-2 control-label">Nhập lại mật khẩu</label>
		<div class="col-sm-8">
			<input type="password" class="form-control" id="txtConfirmPassword" name="txtConfirmPassword">
		</div>
	</div>
	<div class="form-group">
		<label for="txtOldPassword" class="col-sm-2 control-label">Mật khẩu cũ</label>
		<div class="col-sm-8">
			<input type="password" class="form-control" id="txtOldPassword" name="txtOldPassword">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button name="btnChange" type="submit" class="btn btn-success">
				<span class="glyphicon glyphicon-ok"></span>
				Cập nhật
			</button>
		</div>
	</div>
</form>
<script type="text/javascript">
	$('#f').on('submit', function (e) {
		$('#change-failure').hide();
		e.preventDefault();

		var invalid = false;
		var form = this;

		var passwordPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{0,}$/;
		var newPassword = $('#txtNewPassword').val();
		if (newPassword.length == 0 || passwordPattern.test(newPassword) == false) {
			$("#invalid-new-password").show();
			invalid = true;
		}
		else
		{
			$("#invalid-new-password").hide();	
		}

		var confirmPassword = $('#txtConfirmPassword').val();
		if (confirmPassword != newPassword) {
			$("#invalid-confirm-password").show();
			invalid = true;
		}
		else
		{
			$("#invalid-confirm-password").hide();	
		}

		var oldPassword = $('#txtOldPassword').val();
		if (oldPassword.length == 0 || passwordPattern.test(oldPassword) == false) {
			$("#invalid-old-password").show();
			invalid = true;
		}
		else
		{
			$("#invalid-old-password").hide();	
		}
		
		if(invalid == false)
		{
			form.submit();
		}
	});
</script>