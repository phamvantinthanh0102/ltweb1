<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
		  	<div class="panel-heading">
		    	<h3 class="panel-title">Sản phẩm mới nhất</h3>
		  	</div>
		  	<div class="panel-body">
		    	<div class="row">
		    		<?php
						$sql = "select * from products order by startdate desc limit 10";
						$rs = load($sql);
						
						if ($rs->num_rows > 0) :

							while ($row = $rs->fetch_assoc()) :
								$startDate = strtotime($row["StartDate"]);
	    			?>

	    			<div class="col-sm-6 col-md-4">
						<div class="thumbnail">
							<img src="imgs/sp/<?= $row["ProID"] ?>/main.jpg" alt="...">
							<div class="caption">
								<h5 style="height: 40px;"><?= $row["ProName"] ?></h5>
								<h4><?= number_format($row["Price"]) ?></h4>
								<h6 style="color: darkblue;">Ngày nhập: <?= date("j/n/Y", $startDate) ?></h6>
								<div>Xuất xứ: <?= $row["ProSource"] ?></div>
								<div>Số lượng: <?= $row["Quantity"] ?></div>
								<br>
								<p>
									<a href="productDetails.php?id=<?= $row["ProID"] ?>" class="btn btn-primary" role="button">Chi tiết</a>
									<a href="#" class="btn btn-danger add-one-to-cart" role="button" data-id="<?= $row["ProID"] ?>">
										<span class="glyphicon glyphicon-shopping-cart"></span>
										Đặt mua
									</a>
								</p>
							</div>
						</div>
					</div>

					<?php
							endwhile;
						else :
					?>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						Không có sản phẩm nào thỏa điều kiện.
					</div>
					<?php
						endif;
					?>
		    	</div>
		  	</div>
		</div>
	</div>

	<div class="col-md-12">
		<div class="panel panel-success">
		  	<div class="panel-heading">
		    	<h3 class="panel-title">Sản phẩm bán chạy nhất</h3>
		  	</div>
		  	<div class="panel-body">
		    	<div class="row">
		    		<?php
						$sql = "select * from products order by sellcount desc limit 10";
						$rs = load($sql);
						
						if ($rs->num_rows > 0) :

							while ($row = $rs->fetch_assoc()) :
	    			?>

	    			<div class="col-sm-6 col-md-4">
						<div class="thumbnail">
							<img src="imgs/sp/<?= $row["ProID"] ?>/main.jpg" alt="...">
							<div class="caption">
								<h5 style="height: 40px;"><?= $row["ProName"] ?></h5>
								<h4><?= number_format($row["Price"]) ?></h4>
								<h6 style="color: darkgreen;">Số lượng bán: <?= $row["SellCount"] ?></h6>
								<div>Xuất xứ: <?= $row["ProSource"] ?></div>
								<div>Số lượng: <?= $row["Quantity"] ?></div>
								<br>
								<p>
									<a href="productDetails.php?id=<?= $row["ProID"] ?>" class="btn btn-primary" role="button">Chi tiết</a>
									<a href="#" class="btn btn-danger add-one-to-cart" role="button" data-id="<?= $row["ProID"] ?>">
										<span class="glyphicon glyphicon-shopping-cart"></span>
										Đặt mua
									</a>
								</p>
							</div>
						</div>
					</div>

					<?php
							endwhile;
						else :
					?>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						Không có sản phẩm nào thỏa điều kiện.
					</div>
					<?php
						endif;
					?>
		    	</div>
		  	</div>
		</div>
	</div>

	<div class="col-md-12">
		<div class="panel panel-info">
		  	<div class="panel-heading">
		    	<h3 class="panel-title">Sản phẩm được xem nhiều nhất</h3>
		  	</div>
		  	<div class="panel-body">
		    	<div class="row">
		    		<?php
						$sql = "select * from products order by viewcount desc limit 10";
						$rs = load($sql);
						
						if ($rs->num_rows > 0) :

							while ($row = $rs->fetch_assoc()) :
	    			?>

	    			<div class="col-sm-6 col-md-4">
						<div class="thumbnail">
							<img src="imgs/sp/<?= $row["ProID"] ?>/main.jpg" alt="...">
							<div class="caption">
								<h5 style="height: 40px;"><?= $row["ProName"] ?></h5>
								<h4><?= number_format($row["Price"]) ?></h4>
								<h6 style="color: blue;">Lượt xem: <?= $row["ViewCount"] ?></h6>
								<div>Xuất xứ: <?= $row["ProSource"] ?></div>
								<div>Số lượng: <?= $row["Quantity"] ?></div>
								<br>
								<p>
									<a href="productDetails.php?id=<?= $row["ProID"] ?>" class="btn btn-primary" role="button">Chi tiết</a>
									<a href="#" class="btn btn-danger add-one-to-cart" role="button" data-id="<?= $row["ProID"] ?>">
										<span class="glyphicon glyphicon-shopping-cart"></span>
										Đặt mua
									</a>
								</p>
							</div>
						</div>
					</div>

					<?php
							endwhile;
						else :
					?>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						Không có sản phẩm nào thỏa điều kiện.
					</div>
					<?php
						endif;
					?>
		    	</div>
		  	</div>
		</div>
	</div>
</div>
<form id="f" method="post" action="apis/addItemToCart.php">
	<input type="hidden" id="txtProID" name="txtProID">
	<input type="hidden" id="txtQuantity" name="txtQuantity">
</form>
<script type="text/javascript">
	$('.add-one-to-cart').on('click', function() {
		var id = $(this).data('id');
		$('#txtProID').val(id);
	    $('#txtQuantity').val('1');
	    $('#f').submit();
	});
</script>