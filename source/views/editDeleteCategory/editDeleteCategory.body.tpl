<?php
	$updateSuccess = 0;
	$deleteSuccess = 0;
	$catId = "";
	$catName = "";
	if (isset($_POST["txtCmd"])) {
		$cmd = $_POST["txtCmd"];
		$catId = $_POST["txtCatId"];
		$catName = $_POST["txtCatName"];

		if ($cmd == "D") {
			$sql = "delete from categories where CatID = $catId";
			$rs = load($sql);
			if($rs == true)
			{
				$deleteSuccess = 1;
			}
		} else { // $cmd == "U"
			$sql = "update categories set CatName = '$catName' where CatID = $catId";
			$rs = load($sql);
			if($rs == true)	
			{
				$updateSuccess = 1;
			}
		}
	}
	if($deleteSuccess == 1) :
?>
<div class="alert alert-success" role="alert">Bạn đã xóa thành công loại sản phẩm <b><?= $catName ?></b>!</div>
<?php
	endif;
	if($updateSuccess == 1) :
?>
<div class="alert alert-success" role="alert">Bạn đã cập nhật thành công loại sản phẩm <b><?= $catName ?></b>!</div>
<?php
	endif;
?>
<form id="f" method="post" action="">
	<input type="hidden" id="txtCmd" name="txtCmd">
	<input type="hidden" id="txtCatId" name="txtCatId">
	<input type="hidden" id="txtCatName" name="txtCatName">
</form>
<table class="table table-hover">
	<thead>
		<tr>
			<th>Mã loại sản phẩm</th>
			<th>Tên loại sản phẩm</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	<?php
		$sql = "select * from categories";
		$rs = load($sql);
		if ($rs->num_rows > 0) :
			while ($row = $rs->fetch_assoc()) :
	?>
		<tr>
			<td><?= $row["CatID"] ?></td>
			<td><input type="text" class="form-control editCatName" value="<?= $row["CatName"] ?>"></td>
			<td class="text-right">
				<a class="btn btn-xs btn-danger category-remove" data-id="<?= $row["CatID"] ?>" href="javascript:;" role="button">
					<span class="glyphicon glyphicon-remove"></span>
				</a>
				<a class="btn btn-xs btn-primary category-update" data-id="<?= $row["CatID"] ?>" href="javascript:;" role="button">
					<span class="glyphicon glyphicon-ok"></span>
				</a>
			</td>
		</tr>
	<?php
			endwhile;
		endif;
	?>
	</tbody>
</table>

<a href="adminDashboard.php" class="btn btn-default">
	<span class="glyphicon glyphicon-chevron-left"></span>
	Quay lại
</a>

<script type="text/javascript">
	$('.category-remove').on('click', function() {
		
		var s = $(this).closest('tr').find('.editCatName').val();
		$('#txtCatName').val(s);
		$('#modal-cat-name').html(s);

		var id = $(this).data('id');
		$('#txtCatId').val(id);
	    $('#txtCmd').val('D');

	   	$('#delete-modal').modal("show");
	});

	$('.category-update').on('click', function() {

		var s = $(this).closest('tr').find('.editCatName').val();
		$('#txtCatName').val(s);

		var id = $(this).data('id');
		$('#txtCatId').val(id);
	    $('#txtCmd').val('U');

	    $('#f').submit();
	});
</script>

<div class="modal fade" id="delete-modal" role="dialog">
    <div class="modal-dialog">
    
    	<!-- Modal content-->
    	<div class="modal-content">
        	<div class="modal-header">
          		<button type="button" class="close" data-dismiss="modal">&times;</button>
          		<h4 class="modal-title">Xóa loại sản phẩm</h4>
        	</div>
        <div class="modal-body">
          	<p>Bạn có chắc muốn xóa loại sản phẩm <b id="modal-cat-name"></b>?</p>
        </div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-default" id="modal-yes-button">Có</button>
          	<button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
        </div>
      </div>
      
    </div>
</div>

<script type="text/javascript">
	$("#modal-yes-button").on('click', function() {
		$('#f').submit();
	});
</script>