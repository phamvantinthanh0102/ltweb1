<?php
	$orderId = 0;
	$updateSuccess = 0;
	if(isset($_POST["txtOrderId"]))
	{
		$orderId = $_POST["txtOrderId"];
		$statusId = $_POST["txtStatusId"];
		$sql = "update orders set StatusID = $statusId where OrderID = $orderId";
		$rs = load($sql);
		if($rs == true)
		{
			$updateSuccess = 1;
		}
	}
?>
<div class="row">
	<form id="f" method="post" action="">
		<input type="hidden" id="txtOrderId" name="txtOrderId">
		<input type="hidden" id="txtStatusId" name="txtStatusId">
	</form>
	<?php
		if($updateSuccess == 1) :
	?>
	<div class="col-md-12">
		<div class="alert alert-success" role="alert">Bạn đã cập nhật thành công trạng thái của đơn hàng có mã số <b><?= $orderId ?></b>!</div>
	</div>
	<?php
		endif;
	?>
	<?php
		if(isset($_GET["checkOut"]) && $_GET["checkOut"] == "success") :
	?>
	<div class="col-md-12">
		<div class="alert alert-info" role="alert">Chúc mừng bạn đã thanh toán thành công!</div>
	</div>
	<?php
		endif;
	?>
	<?php
		$sql = "select * from orderstatus";
		$rs = load($sql);
		$statusDescription = array();
		$statusColor = array();
		while($row = $rs->fetch_assoc())
		{
			$statusDescription[$row["StatusID"]] = $row["StatusDescription"];
			$statusColor[$row["StatusID"]] = $row["StatusColor"];
		}

		$userId = $_SESSION["current_user"]->f_ID;
		if($_SESSION["current_user"]->f_Permission == 0)
		{
			$sql = "select * from orders where UserID = $userId order by OrderDate desc";	
		}
		else
		{
			$sql = "select * from orders order by OrderDate desc";
		}
		$rs = load($sql);
		if ($rs->num_rows > 0) :
	?>
	<div class="col-md-12">
		<table class="table table-hover">
			<thead>
				<tr>
					<th class="col-md-2">Mã hóa đơn</th>
					<?php
						if($_SESSION["current_user"]->f_Permission == 1) :
					?>
					<th class="col-md-2">Mã khách hàng</th>
					<?php
						endif;
					?>
					<th class="col-md-2">Ngày tạo</th>
					<th class="col-md-2">Thành tiền</th>
					<th class="col-md-2">Tình trạng</th>
					<th class="col-md-2">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
			<?php
				while ($row = $rs->fetch_assoc()) :
					$orderDate = strtotime($row["OrderDate"]);
			?>
				<tr style="background:<?= $statusColor[$row["StatusID"]] ?>;">
					<td><?= $row["OrderID"] ?></td>
					<?php
						if($_SESSION["current_user"]->f_Permission == 1) :
					?>
					<td><?= $row["UserID"] ?></th>
					<?php
						endif;
					?>
					<td><?= date("j/n/Y", $orderDate) ?></td>
					<td><?= number_format($row["Total"]) ?></td>
					<?php
						if($_SESSION["current_user"]->f_Permission == 0) :
					?>
					<td><?= $statusDescription[$row["StatusID"]] ?></td>
					<?php
						else :
					?>
					<td>
						<select class="order-status-select form-control">
						<?php
							foreach($statusDescription as $statusId => $statusDesc) :
						?>
							<option value="<?= $statusId ?>" <?php if($statusId == $row["StatusID"]) echo 'selected' ?>><?= $statusDesc ?></option>
						<?php
							endforeach;
						?>
						</select>
					</td>
					<?php
						endif;
					?>
					<td class="text-right">
						<?php
							if($_SESSION["current_user"]->f_Permission == 1) :
						?>
						<a class="btn btn-xs btn-info order-status-update" data-id="<?= $row["OrderID"] ?>" role="button">
							<span class="glyphicon glyphicon-pencil"></span>
							Cập nhật
						</a>
						<?php
							endif;
						?>
						<a href="orderDetails.php?id=<?= $row["OrderID"] ?>" class="btn btn-xs btn-info" role="button">
							<span class="glyphicon glyphicon-th"></span>
							Chi tiết
						</a>
					</td>
				</tr>
			<?php
				endwhile;
			?>
			</tbody>
		</table>
	</div>
	<?php
		else:
	?>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		Bạn chưa thanh toán hóa đơn nào.
	</div>
	<?php
		endif;
	?>
</div>

<script type="text/javascript">
	$('.order-status-update').on('click', function() {
		var id = $(this).data('id');
		$('#txtOrderId').val(id);
	    
		var statusId = $(this).closest('tr').find('.order-status-select').val();
		$('#txtStatusId').val(statusId);

	    $('#f').submit();
	});
</script>