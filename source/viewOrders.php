<?php

require_once "./lib/db.php";
require_once "./lib/rememberMe.php";

if(!isset($_SESSION["current_user"]))
{
	header("Location: login.php");
}
else
{
	$page_title = "Danh sách hóa đơn";

	$base_filename = basename(__FILE__, '.php');
	$page_body_file = "$base_filename/$base_filename.body.tpl";

	include 'views/_layout.php';
}