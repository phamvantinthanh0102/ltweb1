<?php
	session_start();

	if(!isset($_SESSION["current_user"]))
	{
		if(isset($_COOKIE["current_user"])) {
			$user_id = $_COOKIE["current_user"];
			$sql = "select * from users where f_ID = $user_id";
			$rs = load($sql);
			$_SESSION["current_user"] = $rs->fetch_object();
			setcookie("current_user", $user_id, time() + 86400);
		}
	}
?>