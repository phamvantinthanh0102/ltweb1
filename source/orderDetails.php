<?php

require_once "./lib/db.php";
require_once "./lib/rememberMe.php";

if(!isset($_SESSION["current_user"]))
{
	header("Location: login.php");
}
else
{
	if(!isset($_GET["id"]))
	{
		header("Location: viewOrders.php");
	}
	else
	{
		$page_title = "Xem chi tiết đơn hàng";

		$base_filename = basename(__FILE__, '.php');
		$page_body_file = "$base_filename/$base_filename.body.tpl";

		include 'views/_layout.php';
	}
}