<?php
	require_once "./lib/db.php";
	require_once "./lib/rememberMe.php";

/*
$page_title = "Đăng xuất";

$base_filename = basename(__FILE__, '.php');
$page_body_file = "$base_filename/$base_filename.body.tpl";

include 'views/_layout.php';
*/

if (isset($_SESSION["current_user"])) {
	unset($_SESSION["current_user"]);

	setcookie("current_user", "", time() - 3600);

	if (isset($_SESSION["cart"])) {
		unset($_SESSION["cart"]);
	}
}

header('Location: login.php');

?>